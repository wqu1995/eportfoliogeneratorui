/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var jsonData;
var i =0;
var slideplay = 0;
var myVar;
function addTxtComponent(data){
    var txtComponent = document.createElement("p");
    for(var x=0; x< data.component_srcs.length; x++){
        if(data.component_srcs[x].text_type === "Header"){
                var txtHeader = document.createElement("h1");
                for(var y = 0; y<data.component_srcs[x].text_srcs.length; y++){
                if(data.component_srcs[x].text_srcs[y].text_src !=null){
                    txtHeader.innerHTML += data.component_srcs[x].text_srcs[y].text_src;
                }
                if((y<data.component_srcs[x].text_hyperlink_srcs.length) && (data.component_srcs[x].text_hyperlink_srcs[y] != null)){
                    var hyperlink = document.createElement("a");
                    hyperlink.innerHTML = data.component_srcs[x].text_hyperlink_srcs[y].text_hyperlink_src;
                    hyperlink.setAttribute("href", data.component_srcs[x].text_hyperlink_dists[y].text_hyperlink_dist);
                    txtHeader.appendChild(hyperlink);
                }
                }
                document.getElementById("main_container").appendChild(txtHeader);

        }
        else if(data.component_srcs[x].text_type === "Paragraph"){
            var txt = document.createElement("t");
            for(var y = 0; y<data.component_srcs[x].text_srcs.length; y++){
                if(data.component_srcs[x].text_srcs[y].text_src !=null){
                    txt.innerHTML += data.component_srcs[x].text_srcs[y].text_src;
                }
                if((y<data.component_srcs[x].text_hyperlink_srcs.length) && (data.component_srcs[x].text_hyperlink_srcs[y] != null)){
                    var hyperlink = document.createElement("a");
                    hyperlink.innerHTML = data.component_srcs[x].text_hyperlink_srcs[y].text_hyperlink_src;
                    hyperlink.setAttribute("href", data.component_srcs[x].text_hyperlink_dists[y].text_hyperlink_dist);
                    txt.appendChild(hyperlink);
                }
            }
            txtComponent.setAttribute('style', 'font-family: "'+data.component_srcs[x].text_font+'";');
            txtComponent.appendChild(txt);
        }
        else if(data.component_srcs[x].text_type === "List"){
            var list = document.createElement("ul");
            list.setAttribute('style', 'list-style-type: disc');
            for(var v=0; v< data.component_srcs[x].text_srcs.length; v++){
                var listNode = document.createElement("li");
                for(var w = 0; w< data.component_srcs[x].text_srcs[v].list_srcs.length; w++){
                    if(data.component_srcs[x].text_srcs[v].list_srcs[w].list_src != null){
                        listNode.innerHTML+=data.component_srcs[x].text_srcs[v].list_srcs[w].list_src;
                    }
                    if((w<data.component_srcs[x].text_srcs[v].list_hyperlink_srcs.length) && (data.component_srcs[x].text_srcs[v].list_hyperlink_srcs[w]!=null)){
                        var hyperlink =document.createElement("a");
                        hyperlink.innerHTML  = data.component_srcs[x].text_srcs[v].list_hyperlink_srcs[w].list_hyperlink_src;
                        hyperlink.setAttribute("href",data.component_srcs[x].text_srcs[v].list_hyperlink_dists[w].list_hyperlink_dist);
                        listNode.appendChild(hyperlink);
                    }
                }
                list.appendChild(listNode);
            }
            txtComponent.appendChild(list);
        }
    }
   return txtComponent;

}
function addImgComponent(data){
    var imgComponentWarpper = document.createElement("figure");
    var imgComponent = document.createElement("img");
    var imgCaption = document.createElement("figcaption");
    imgComponent.setAttribute('src', "img/"+data.component_srcs[0].img_src.substring(data.component_srcs[0].img_src.lastIndexOf("\\")+1));
    if(data.component_srcs[0].img_option =="none"){
        imgComponent.setAttribute('style', 'width:'+data.component_srcs[0].img_width+'%; height:' +data.component_srcs[0].img_height +'%;');
    }
    else if(data.component_srcs[0].img_option == "float left"){
        imgComponent.setAttribute('style', 'width:'+data.component_srcs[0].img_width+'%; height:' +data.component_srcs[0].img_height +'%; float: left;');
        imgCaption.setAttribute('style', 'float:left');

    }
    else if(data.component_srcs[0].img_option == "float right"){
        imgComponent.setAttribute('style', 'width:'+data.component_srcs[0].img_width+'%; height:' +data.component_srcs[0].img_height +'%; float: right;');
        imgCaption.setAttribute('style', 'float:right');

        
    }
    imgCaption.innerHTML = data.component_srcs[0].img_caption;
    imgComponentWarpper.appendChild(imgComponent);
    imgComponentWarpper.appendChild(imgCaption);    
    return imgComponentWarpper;
}
function addVideoComponent(data){
    var videoP = document.createElement("p");
    var videoComponent = document.createElement("video");
    videoComponent.setAttribute('width',data.component_srcs[0].video_width+"%");
    videoComponent.setAttribute('height',data.component_srcs[0].video_height+"%");
    var videoComponentSrc = document.createElement("source");
    videoComponentSrc.type = "video/"+data.component_srcs[0].video_src.substring(data.component_srcs[0].video_src.lastIndexOf(".")+1);
    videoComponentSrc.src = "video/"+data.component_srcs[0].video_src.substring(data.component_srcs[0].video_src.lastIndexOf("\\")+1);
    var videoCaption = document.createElement("p");
    videoCaption.innerHTML = "chicken";
    videoCaption.setAttribute('style', 'text-align: center;');
    videoComponent.appendChild(videoComponentSrc);
    videoComponent.setAttribute('controls','controls');
    videoP.appendChild(videoComponent);
    videoP.appendChild(videoCaption);
    return videoP;
}

function addSlideShowComponent(data){
    var a = 0;
    var slideShowComponentWarpper = document.createElement("figure");
    var slideShowComponent = document.createElement("img");
    var slideShowCaption = document.createElement("figcaption");
    slideShowComponent.setAttribute('src',"img/"+data.component_srcs[0].slideshow_src.substring(data.component_srcs[0].slideshow_src.lastIndexOf("\\")+1));
    slideShowComponent.setAttribute('style', 'width:100%; height:450px');
    slideShowCaption.innerHTML = data.component_srcs[0].slideshow_caption;
    slideShowCaption.setAttribute('style','text-align: center;');
    var previousButton = document.createElement("button");
    previousButton.innerHTML = "Previous";
    previousButton.setAttribute("style", "text_align:center;");
        var PlayButton = document.createElement("button");
    PlayButton.innerHTML = "Play";
    PlayButton.setAttribute("style", "text_align:center;");
        var nextButton = document.createElement("button");
    nextButton.innerHTML = "Next";
    nextButton.setAttribute("style", "text_align:center;");
    
    $(previousButton).click(function(){
        if(a ===0){
            a = data.component_srcs.length-1;
        }
        else{
            a--;
        }
        $(slideShowComponent).fadeOut(function(){
                $(this).attr('src', "img/"+data.component_srcs[a].slideshow_src.substring(data.component_srcs[a].slideshow_src.lastIndexOf("\\")+1));

        }).fadeIn();
        $(slideShowCaption).fadeOut(function(){
            $(this).text(data.component_srcs[a].slideshow_caption);
        }).fadeIn();
    });
    $(PlayButton).click(function(){
        if(slideplay ===0){
            $(this).text(slideplay);
            slideplay=1;

            myVar = setInterval(function(){
            if(a === data.component_srcs.length-1){
                a = 0;
            }
            else{
                a++;
            }
            $(slideShowComponent).fadeOut(function(){
                $(this).attr('src', "img/"+data.component_srcs[a].slideshow_src.substring(data.component_srcs[a].slideshow_src.lastIndexOf("\\")+1));
            }).fadeIn();
            $(slideShowCaption).fadeOut(function(){
                $(this).text(data.component_srcs[a].slideshow_caption);
            }).fadeIn();
        }, 3000);
        }
        if(slideplay ===1){
            $(this).text(slideplay);
            slideplay=0;

            clearInterval();
        }
    });
    $(nextButton).click(function(){
        if(a ===data.component_srcs.length-1){
            a = 0;
        }
        else{
            a++;
        }
        $(slideShowComponent).fadeOut(function(){
                $(this).attr('src', "img/"+data.component_srcs[a].slideshow_src.substring(data.component_srcs[a].slideshow_src.lastIndexOf("\\")+1));

        }).fadeIn();
        $(slideShowCaption).fadeOut(function(){
            $(this).text(data.component_srcs[a].slideshow_caption);
        }).fadeIn();
    });
    slideShowComponentWarpper.appendChild(slideShowComponent);
    slideShowComponentWarpper.appendChild(slideShowCaption);
   slideShowComponentWarpper.appendChild(previousButton);
   slideShowComponentWarpper.appendChild(PlayButton);
   slideShowComponentWarpper.appendChild(nextButton);

    //$(slideShowComponent).click(function(){
        /**/
        return slideShowComponentWarpper;
    //});
}

function addNavigationBar(data){
    var list = document.createElement("ul");
    list.setAttribute('style', 'list-style-type: disc');
    list.setAttribute('id','navigation_bar');
    for(var n=0; n< data.length; n++){
        var listNode = document.createElement("li");
        var dist = document.createElement("a");
                    dist.setAttribute('tabindex', n);
         dist.setAttribute('href', data[n].navigation_dist);
                 dist.innerHTML= data[n].navigation_src;
        listNode.appendChild(dist);
        list.appendChild(listNode);        
    }
    return list;    
}

$(document).ready(function(){
    var path = window.location.pathname;
    var page = path.substring(path.lastIndexOf("/")+1, path.lastIndexOf("."));
    $.getJSON("data/"+page+".json", function(data){
        jsonData = data;
        document.title = jsonData.page_title;
        document.getElementById('template').setAttribute('href', 'css/template/'+jsonData.page_layout+'.css');
        document.getElementById('color').setAttribute('href', 'css/color/'+jsonData.page_color+'.css');
        document.getElementById('font').setAttribute('href', 'css/font/'+jsonData.page_font+'.css');
        if(jsonData.banner_img_src !== "null"){
            document.getElementById("banner_src").src= "img/"+jsonData.page_banner.substring(jsonData.page_banner.lastIndexOf("\\")+1);
        }
        document.getElementById("name_data").innerHTML=jsonData.page_student_name;
        document.getElementById("page_title").innerHTML=jsonData.page_title;
        document.getElementById("navigation_container").appendChild(addNavigationBar(jsonData.navigation));
        
        for(i = 0; i<jsonData.page_components.length; i++){
            if(jsonData.page_components[i].component_type === "text"){
               document.getElementById("main_container").appendChild(addTxtComponent(jsonData.page_components[i]));
            }
            else if(jsonData.page_components[i].component_type ==="image"){
                document.getElementById("main_container").appendChild(addImgComponent(jsonData.page_components[i]));
            }
            else if(jsonData.page_components[i].component_type ==="video"){
                document.getElementById("main_container").appendChild(addVideoComponent(jsonData.page_components[i]));
            }
            else if(jsonData.page_components[i].component_type === "slideshow"){
                document.getElementById("main_container").appendChild(addSlideShowComponent(jsonData.page_components[i]));
            }
        }
        var footer = document.createElement("p");
        footer.innerHTML = "<br></br><hr></hr>"+ jsonData.page_footer;
        //footer.innerHTML = "<br></br><hr></hr>Web page created and maintained<br></br>by ePortfolio Generator Application";
        footer.setAttribute('style','font-size: 0.5em; text-align:center');
       // footer.setAttribute('style','text-align: center');
        document.getElementById("main_container").appendChild(footer);
                window.onload = function() {
            if(!window.location.hash) {
            window.location = window.location + '#loaded';
            window.location.reload();
            }
        }
    });

});