/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epg.model;

import static epg.StartupConstants.CSS_TEXT_SELECTION;
import java.util.ArrayList;
import java.util.Optional;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

/**
 *
 * @author wqu
 */
public class EportfolioTextComponent extends EportfolioComponent{
    int i=0;
    String srcType;
    String font = "Comfortaa";
     ArrayList<String> srcs;
     ArrayList<String> hyperlinkSrcs;
     ArrayList<String> hyperlinkDist;
    String header;
    String paragraph;
            String src;
        String dist;
    ArrayList<String> lists;
    ObservableList<TextField> listTextFields;
    ObservableList<EportfolioList> listSrcs;
    TextField headerTextField;
    TextField headerEditTextField;
    TextArea paragraphTextArea;
    TextArea paragraphEditTextArea;
            HBox listTextBox;
    HBox selected;
    boolean edit;
    boolean listHyperlink = false;

    
    public EportfolioTextComponent(String initType) {
        super(initType);
        lists= new ArrayList();
        listTextFields = FXCollections.observableArrayList();
        listSrcs = FXCollections.observableArrayList();
        srcs = new ArrayList();
        hyperlinkSrcs = new ArrayList();
        hyperlinkDist = new ArrayList();

    }
    
    public ObservableList<EportfolioList>getListSrcs(){
        return listSrcs;
    }
    public String getSrcType(){
        return srcType;
    }
    public ArrayList<String> getSrcs(){
        return srcs;
    }
    public void setSrcType(String initSrcType){
        srcType = initSrcType;
    }
    public void setFont(String initFont){
        font = initFont;
    }
    public void setSrcs(String initSrc){
        srcs.add(initSrc);
    }
    public boolean manageTextComponent(){

        HBox contentBox = new HBox();
        Dialog textComponentDialog = new Dialog();
        textComponentDialog.setTitle("Text Component Manager");
        ButtonType buttonYes = new ButtonType("Yes");
        textComponentDialog.getDialogPane().getButtonTypes().addAll(buttonYes, ButtonType.CANCEL);
        textComponentDialog.setResizable(true);

        GridPane grid = new GridPane();

        ComboBox textTypeBox = new ComboBox();
        ComboBox fontBox = new ComboBox();
        textTypeBox.getItems().addAll("Header","Paragraph","List");
        textTypeBox.setValue(srcType);
        textTypeBox.setOnAction(e->{
            if(textTypeBox.getValue().equals("Header")){
                srcType = (String) textTypeBox.getValue();
                contentBox.getChildren().clear();
                contentBox.getChildren().add(new Label("Enter Header Text: "));
                headerTextField = new TextField();
                contentBox.getChildren().add(headerTextField);
                textComponentDialog.setHeight(170);
                textComponentDialog.setWidth(440);
                
            }
            else if(textTypeBox.getValue().equals("Paragraph")){
                srcType = (String) textTypeBox.getValue();
                contentBox.getChildren().clear();
                contentBox.getChildren().add(new Label("Enter Paragraph Text: "));
                paragraphTextArea = new TextArea();
                paragraphTextArea.setPrefSize(300, 100);
                paragraphTextArea.setPrefRowCount(100);
                paragraphTextArea.setWrapText(true);
                contentBox.getChildren().add(paragraphTextArea);
                textComponentDialog.setHeight(250);
                textComponentDialog.setWidth(556);
                VBox fontDataBox = new VBox();
                fontBox.getItems().addAll("Comfortaa","Handlee","Indie Flower","Josefin Sans","Quicksand");
                fontBox.setValue(font);
                fontBox.setOnAction(f->{
                    setFont((String) fontBox.getValue());
                });
                fontDataBox.getChildren().addAll(new Label("Font: "), fontBox);
                contentBox.getChildren().add(fontDataBox);
            }
            else if(textTypeBox.getValue().equals("List")){
                srcType = (String) textTypeBox.getValue();
                textComponentDialog.setHeight(170);
                textComponentDialog.setWidth(440);
                contentBox.getChildren().clear();
                VBox listControlBox = new VBox();
                Button addListButton = new Button("Add List Item");
                Button removeListButton = new Button ("Remove Selected List Item");
                listControlBox.getChildren().addAll(addListButton, removeListButton);
                GridPane listTextPane = new GridPane();
                ScrollPane listScrollPane = new ScrollPane(listTextPane);
                listScrollPane.setFitToWidth(true);
                contentBox.getChildren().addAll(listControlBox, listScrollPane);
                addListButton.setOnAction(l->{
                    listTextBox = new HBox();
                    TextField listText = new TextField();
                    listTextFields.add(listText);
                    CheckBox chekBox =new CheckBox();
                    chekBox.setOnAction(x->{
                        if(chekBox.isSelected()){
                            if(selected ==null){
                                selected = (HBox)chekBox.getParent();
                            }
                            else if(selected !=null){
                                ((CheckBox)selected.getChildren().get(1)).setSelected(false);
                                selected = (HBox)chekBox.getParent();
                            }
                        }
                    });
                    listTextBox.getChildren().addAll(listText, chekBox);
                    listTextPane.add(listTextBox, 0, i);
                    i++;
                    textComponentDialog.setHeight(textComponentDialog.getHeight()+20);
                });
                removeListButton.setOnAction(r->{
                        listTextPane.getChildren().remove(selected);
                        listTextFields.remove((TextField)selected.getChildren().get(0));
                        selected = null;
                });

            }
        });
        HBox typeBox = new HBox();
        typeBox.getChildren().addAll(new Label("Text Type: "), textTypeBox);
        grid.add(typeBox, 0, 0);
        grid.add(contentBox, 0, 1);
        textComponentDialog.getDialogPane().setContent(grid);

        Optional<ButtonType>result = textComponentDialog.showAndWait();
        if(result.isPresent() && (srcType != null) && (result.get() != ButtonType.CANCEL)){
          if (srcType.equals("Header")){
              srcs.add(headerTextField.getText());
          }
          else if (srcType.equals("Paragraph")){
              srcs.add(paragraphTextArea.getText()) ;
          }
          else if(srcType.equals("List")){
              for (TextField listTextField : listTextFields){
                  EportfolioList list = new EportfolioList(listTextField.getText());
                  listSrcs.add(list);
              }
          }
          return true;
        }
        return false;
    }

    public void edtTextComponentManager(){
        if((!hyperlinkSrcs.isEmpty()) ||(listHyperlink)){
            Alert alert = new Alert(AlertType.CONFIRMATION);
            alert.setTitle("Warning!");
            alert.setContentText("This text Component contains hyperlink items. editing this component will delete all the hyperlinks");
            alert.getButtonTypes().setAll(ButtonType.OK, ButtonType.CANCEL);
            Optional<ButtonType> result = alert.showAndWait();
            if(result.get() == ButtonType.OK){
                edit = true;
            }
            else{
                edit = false;
            }
        }
        else{
            edit = true;
        }
        if(edit){
        Dialog editTextDialog = new Dialog();
        ComboBox fontBox = new ComboBox();

        editTextDialog.setTitle("Edit text Manager");
        ButtonType buttonYes = new ButtonType("Yes");
        editTextDialog.getDialogPane().getButtonTypes().addAll(buttonYes, ButtonType.CANCEL);
        editTextDialog.setResizable(true);
        
        if (srcType.equals("Header")){
            VBox headerEditBox = new VBox();
            Label headerEditLabel = new Label("Edit this header: ");
            headerEditTextField = new TextField();
            for(int x = 0; x< srcs.size(); x++){
                if(srcs.get(x) !=null){
                    headerEditTextField.setText(headerEditTextField.getText()+srcs.get(x));
                }
                if((x<hyperlinkSrcs.size()) && (hyperlinkSrcs.get(x)!=null)){
                    headerEditTextField.setText(headerEditTextField.getText()+hyperlinkSrcs.get(x));
                }
            }
            headerEditBox.getChildren().addAll(headerEditLabel, headerEditTextField);
            editTextDialog.getDialogPane().setContent(headerEditBox);
            
        }
        else if (srcType.equals("Paragraph")){
            VBox paragraphEditBox = new VBox();
            Label paragraphEditLabel = new Label("Edit this paragraph: ");
            paragraphEditTextArea = new TextArea();
            for(int x = 0; x< srcs.size(); x++){
                if(srcs.get(x) !=null){
                    paragraphEditTextArea.setText(paragraphEditTextArea.getText()+srcs.get(x));
                }
                if((x<hyperlinkSrcs.size()) && (hyperlinkSrcs.get(x)!=null)){
                    paragraphEditTextArea.setText(paragraphEditTextArea.getText()+hyperlinkSrcs.get(x));
                }
            }
            paragraphEditTextArea.setPrefSize(300, 100);
                            paragraphEditTextArea.setPrefRowCount(100);
                paragraphEditTextArea.setWrapText(true);
                VBox fontDataBox = new VBox();
                fontBox.getItems().addAll("Comfortaa","Handlee","Indie Flower","Josefin Sans","Quicksand");
                fontBox.setValue(getFont());
                fontBox.setOnAction(f->{
                    setFont((String) fontBox.getValue());
                });
                fontDataBox.getChildren().addAll(new Label("Font: "), fontBox);

            paragraphEditBox.getChildren().addAll(paragraphEditLabel, paragraphEditTextArea, fontDataBox);
  
            editTextDialog.getDialogPane().setContent(paragraphEditBox);
        }
        else if (srcType.equals("List")){
            i =0;

            VBox ListEditBox = new VBox();
            Label ListEditLabel = new Label("Edit these List: ");
            HBox listContentBox = new HBox();
            VBox listControlBox = new VBox();
            GridPane listPane = new GridPane();
            ScrollPane listScrollPane = new ScrollPane(listPane);
            listTextFields.clear();
            for(EportfolioList list : listSrcs){
                    listTextBox = new HBox();
                    TextField listText = new TextField();
                    for(int x = 0; x < list.getSrcs().size(); x++){
                        if(list.getSrcs().get(x) != null){
                            listText.setText(listText.getText()+list.getSrcs().get(x));
                        }
                        if((x < list.getHyperlinkSrcs().size()) && (list.getHyperlinkSrcs().get(x)!=null)){
                            listText.setText(listText.getText()+list.getHyperlinkSrcs().get(x));
                        }
                    }
                    listTextFields.add(listText);
                    CheckBox chekBox =new CheckBox();
                    chekBox.setOnAction(x->{
                        if(chekBox.isSelected()){
                            if(selected ==null){
                                selected = (HBox)chekBox.getParent();
                            }
                            else if(selected !=null){
                                ((CheckBox)selected.getChildren().get(1)).setSelected(false);
                                selected = (HBox)chekBox.getParent();
                            }
                        }
                    });
                    listTextBox.getChildren().addAll(chekBox, listText);

                listPane.add(listTextBox, 0, i);
                i++;

            }
            Button addListButton = new Button("Add list item");
             addListButton.setOnAction(l->{
                    listTextBox = new HBox();
                    TextField listText = new TextField();
                    listTextFields.add(listText);
                    CheckBox chekBox =new CheckBox();
                    chekBox.setOnAction(x->{
                        if(chekBox.isSelected()){
                            if(selected ==null){
                                selected = (HBox)chekBox.getParent();
                            }
                            else if(selected !=null){
                                ((CheckBox)selected.getChildren().get(1)).setSelected(false);
                                selected = (HBox)chekBox.getParent();
                            }
                        }
                    });
                    listTextBox.getChildren().addAll(chekBox, listText);
                    
                    listPane.add(listTextBox, 0, i);
                    i++;
                    editTextDialog.setHeight(editTextDialog.getHeight()+25);
                });
                Button removeListButton = new Button("Remove list item");
                removeListButton.setOnAction(r->{
                    listPane.getChildren().remove(selected);
                    listTextFields.remove((TextField)selected.getChildren().get(1));
                    
                    selected = null;
                });
                listControlBox.getChildren().addAll(addListButton, removeListButton);
            listContentBox.getChildren().addAll(listScrollPane, listControlBox);
            ListEditBox.getChildren().addAll(ListEditLabel, listContentBox);
            editTextDialog.getDialogPane().setContent(ListEditBox);
        }
        Optional<ButtonType>results = editTextDialog.showAndWait();
        if(results.isPresent() &&(results.get() != ButtonType.CANCEL)){
            srcs.clear();
            
            hyperlinkSrcs.clear();
            hyperlinkDist.clear();
            switch (srcType) {
                case "Header":
                    srcs.add(headerEditTextField.getText());
                    break;
                case "Paragraph":
                    srcs.add(paragraphEditTextArea.getText()) ;
                    break;
                case "List":{
                    listSrcs.clear();
                    listHyperlink = false;
                    for (TextField listTextField : listTextFields){
                        EportfolioList list = new EportfolioList(listTextField.getText());
                        listSrcs.add(list);
                    }  
                }break;
            }
        }
        }
       
    }
    
    public void hyperlinkManager() {
        Dialog hyperlinkDialog = new Dialog();
        hyperlinkDialog.setTitle("Hyperlink Manager");
        ButtonType buttonYes = new ButtonType("Yes");
        hyperlinkDialog.getDialogPane().getButtonTypes().addAll(buttonYes, ButtonType.CANCEL);
        hyperlinkDialog.setResizable(true);
        
        GridPane grid = new GridPane();
        if(srcType.equals("Paragraph") || srcType.equals("Header")){
            Label srcLabel = new Label("Text Source: ");
            TextField srcTextField = new TextField();
            grid.add(new Label("Highlight the text that you wish to become a hyperlink:"), 0, 0);

            TextArea text = new TextArea();
            for(int x = 0; x< srcs.size(); x++){
                if(srcs.get(x)!= null){
                    text.setText(text.getText()+srcs.get(x));
                }
                if((x < hyperlinkSrcs.size()) && (hyperlinkSrcs.get(x) !=null)){
                    text.setText(text.getText()+hyperlinkSrcs.get(x));
                }
            }
            text.setWrapText(true);
            text.setEditable(false);
            text.setOnMouseClicked(e->{
                srcTextField.setText(text.getSelectedText());
            });
            grid.add(text, 0, 1);
            Label distLabel = new Label("Website link: ");
            TextField distTextField = new TextField();
            grid.add(srcLabel, 0, 2);
            grid.add(srcTextField, 0, 3);
            grid.add(distLabel, 0, 4);
            grid.add(distTextField, 0, 5);
            Button reset = new Button("Rest all the hyperlinks");
            reset.setOnAction(e->{
                srcs.clear();
                hyperlinkSrcs.clear();
                hyperlinkDist.clear();
                srcs.add(text.getText());
            });
            grid.add(reset, 0, 6);
            hyperlinkDialog.getDialogPane().setContent(grid);
            Optional<ButtonType>result = hyperlinkDialog.showAndWait();
            if(result.isPresent() &&(result.get()!= ButtonType.CANCEL)){
                String temp = text.getText();
                int s = srcs.size();
                for(int x = 0; x < s; x++){
                    if(srcs.get(x).contains(srcTextField.getText())){
                        temp = srcs.get(x);
                        srcs.set(x, temp.substring(0, temp.indexOf(srcTextField.getText())));
                        hyperlinkSrcs.add(x, srcTextField.getText());
                      //  hyperlinkSrcs.add(srcTextField.getText());
                        String b = temp.substring(temp.indexOf(srcTextField.getText())+srcTextField.getText().length());
                        if(x<s){
                        srcs.add(x+1, b);
                        }
                        else{
                            srcs.add(b);
                        }
                        String dist;
                        if(!distTextField.getText().contains("http://")){
                            dist = "http://"+distTextField.getText();
                        }
                        else{
                            dist = distTextField.getText();
                        }
                        hyperlinkDist.add(dist);
                    }
                }

            }
        }
        else if(srcType.equals("List")){
            ObservableList<HBox>dataContainerList = FXCollections.observableArrayList();
            grid.add(new Label("Highlight the text that you wish to become a hyperlink:"), 0, 0);
            HBox listDataContainer;
            int x = 1;
            for(EportfolioList list : listSrcs){
                listDataContainer = new HBox();
                VBox srcBox = new VBox();
                VBox hyperlinkSrcBox = new VBox();
                VBox hyperlinkDistBox = new VBox();
                TextField srcTextField = new TextField();
                TextField hyperlinkTextField = new TextField();
                TextField hyperlinkDist = new TextField();
                srcTextField.setOnMouseClicked(e->{
                    hyperlinkTextField.setText(srcTextField.getSelectedText());
                });
                srcTextField.setEditable(false);
                for(int y = 0; y< list.getSrcs().size(); y++){
                        if(list.getSrcs().get(y) != null){
                            srcTextField.setText(srcTextField.getText()+list.getSrcs().get(y));
                        }
                        if((y < list.getHyperlinkSrcs().size()) && (list.getHyperlinkSrcs().get(y)!=null)){
                            srcTextField.setText(srcTextField.getText()+list.getHyperlinkSrcs().get(y));
                        }
                }
                srcBox.getChildren().addAll(new Label("list source:"), srcTextField);
                hyperlinkSrcBox.getChildren().addAll(new Label("Highlighted source"), hyperlinkTextField);
                hyperlinkDistBox.getChildren().addAll(new Label("website address"), hyperlinkDist);
                listDataContainer.getChildren().addAll(srcBox,hyperlinkSrcBox,hyperlinkDistBox);
                dataContainerList.add(listDataContainer);
                grid.add(listDataContainer, 0, x);
                x++;
                
            }
                        Button reset = new Button("Rest all the hyperlinks");
            reset.setOnAction(e->{
                listSrcs.clear();
                for(HBox data : dataContainerList){
                    TextField srcData = (TextField) ((VBox)data.getChildren().get(0)).getChildren().get(1);
                    listSrcs.add(new EportfolioList(srcData.getText()));
                }
            });
            grid.add(reset, 0, x);
            hyperlinkDialog.getDialogPane().setContent(grid);
            Optional<ButtonType>result = hyperlinkDialog.showAndWait();
            if(result.isPresent() &&(result.get()!= ButtonType.CANCEL)){
                int m = 0;
                for(HBox data : dataContainerList){
                    TextField srcData = (TextField) ((VBox)data.getChildren().get(0)).getChildren().get(1);
                    TextField hyperlinkSrcData = (TextField) ((VBox)data.getChildren().get(1)).getChildren().get(1);
                    TextField hyperlinkDistData = (TextField) ((VBox)data.getChildren().get(2)).getChildren().get(1);
                    EportfolioList temp = listSrcs.get(m);
                    String tempData;
                    int n = temp.getSrcs().size();
                    for(int l = 0; l < n; l++){
                        if(temp.getSrcs().get(l).contains(hyperlinkSrcData.getText())){
                            tempData = temp.getSrcs().get(l);
                            temp.getSrcs().set(l, tempData.substring(0, tempData.indexOf(hyperlinkSrcData.getText())));
                            temp.getHyperlinkSrcs().add(l, hyperlinkSrcData.getText());
                            if(l<n){
                               temp.getSrcs().add(l+1, tempData.substring(tempData.indexOf(hyperlinkSrcData.getText())+hyperlinkSrcData.getText().length()));
                            }
                            else{
                                temp.getSrcs().add(tempData.substring(tempData.indexOf(hyperlinkSrcData.getText())+hyperlinkSrcData.getText().length()));
                            }
                            temp.getHyperlinkDists().add(hyperlinkDistData.getText());
                            listHyperlink = true;
                        }
                    }
                    m++;
                }
            }
            
        }

    }
    
    public ArrayList<String> getHyperlinks(){
        return hyperlinkSrcs;
    }
    public ArrayList<String> getHyperlinkDists(){
        return hyperlinkDist;
    }
    public String getFont() {
        return font;
    }

}
