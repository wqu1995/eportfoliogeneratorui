/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epg.model;

import java.io.File;
import java.util.ArrayList;
import java.util.Optional;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;

/**
 *
 * @author wqu
 */
public class EportfolioSlideshowComponent extends EportfolioComponent{
    ObservableList<EportfolioSlide> srcs;
    ObservableList<Label>imgSrcs;
    ObservableList<TextField>imgCaptions;
    ObservableList<VBox> slideshowDatas;
    VBox selected;
    int i = 0;
    File img;
    public EportfolioSlideshowComponent(String initType) {
        super(initType);
        srcs = FXCollections.observableArrayList();
        imgSrcs = FXCollections.observableArrayList();
        imgCaptions = FXCollections.observableArrayList();
                slideshowDatas = FXCollections.observableArrayList();
    }
    
    public ObservableList<EportfolioSlide> getSrcs(){
        return srcs;
    }

    public boolean manageSlideshowComponent() {

        Dialog slideshowComponentDialog = new Dialog();

        slideshowComponentDialog.setTitle("Slideshow Component Manager");
        ButtonType buttonYes = new ButtonType("Yes");
        slideshowComponentDialog.getDialogPane().getButtonTypes().addAll(buttonYes, ButtonType.CANCEL);
        slideshowComponentDialog.setResizable(true);
        
        HBox dialogBox = new HBox();
        VBox slideshowControlBox = new VBox();
        Button addSlide = new Button("Add Slide");
        Button removeSlide = new Button("Remove Slide");
        Button moveUpSlide = new Button ("Move Slide up");
        Button moveDownSlide = new Button("Move Slide down");
        slideshowControlBox.getChildren().addAll(addSlide, removeSlide, moveUpSlide, moveDownSlide);
        
        GridPane slidePane = new GridPane();
        ScrollPane slideScrollPane = new ScrollPane(slidePane);
        dialogBox.getChildren().add(slideshowControlBox);
        dialogBox.getChildren().add(slideScrollPane);
        
        addSlide.setOnAction(e->{
                    slideshowComponentDialog.setHeight(300);
                slideshowComponentDialog.setWidth(800);
            VBox slideDataComboBox = new VBox();
            HBox slideDataBox = new HBox();
            HBox captionDataBox = new HBox();
            Label imageSelectionLabel = new Label("Select Image: ");
            Button imageSelectionButton = new Button("Image");
            Label imageSrcLabel = new Label();
            Label imageCaptionLabel = new Label("Enter Image Caption: ");
            TextField imageCaptionTextField = new TextField();
            imgCaptions.add(imageCaptionTextField);
            CheckBox checkBox = new CheckBox();
            checkBox.setOnAction(x->{
                if(checkBox.isSelected()){
                    if(selected ==null){
                        selected = (VBox)checkBox.getParent().getParent();
                        if(GridPane.getRowIndex(selected) ==0){
                            moveUpSlide.setDisable(true);
                            moveDownSlide.setDisable(false);
                        }
                        else if(GridPane.getRowIndex(selected) == i-1){
                            moveDownSlide.setDisable(true);
                            moveUpSlide.setDisable(false);
                        }
                        else{
                            moveUpSlide.setDisable(false);
                            moveDownSlide.setDisable(false);
                        }
                    }
                    else if(selected !=null){
                       // ((CheckBox)selected.getChildren().get(0))
                        HBox temp = (HBox)selected.getChildren().get(0);
                        CheckBox che = (CheckBox)temp.getChildren().get(3);
                        che.setSelected(false);
                        selected = (VBox)checkBox.getParent().getParent();
                                                if(GridPane.getRowIndex(selected) ==0){
                            moveUpSlide.setDisable(true);
                        }
                        else{
                            moveUpSlide.setDisable(false);
                        }
                    }
                }
                else if(!(checkBox.isSelected())){
                    selected = null;
                }
            });
            slideDataBox.getChildren().addAll(imageSelectionLabel, imageSelectionButton, imageSrcLabel, checkBox);
            captionDataBox.getChildren().addAll(imageCaptionLabel, imageCaptionTextField);
            slideDataComboBox.getChildren().addAll(slideDataBox, captionDataBox);
            slideshowDatas.add(slideDataComboBox);
            slidePane.add(slideDataComboBox, 0, i);
            i++;
            imageSelectionButton.setOnAction(f->{
                FileChooser fileChooser = new FileChooser();
                fileChooser.setTitle("Choice Image");
                fileChooser.setInitialDirectory(new File(System.getProperty("user.home")));
        
              fileChooser.getExtensionFilters().addAll(
                    new FileChooser.ExtensionFilter("JPG", "*.jpg"),
                    new FileChooser.ExtensionFilter("PNG", "*.png")
                );
                img = fileChooser.showOpenDialog(null);
                if(img != null){
                    imageSrcLabel.setText(img.getPath());
                    imgSrcs.add(imageSrcLabel);

                }     
            });

        });
        removeSlide.setOnAction(r->{
            slidePane.getChildren().remove(selected);
            imgSrcs.remove((Label)((HBox)selected.getChildren().get(0)).getChildren().get(2));
            imgCaptions.remove((TextField)((HBox)selected.getChildren().get(1)).getChildren().get(1));
            selected = null;
        });
        moveUpSlide.setOnAction(u->{
            int k = GridPane.getRowIndex(selected);
            if(k> 0){
                VBox temp = slideshowDatas.get(k-1);
                slideshowDatas.set(k-1, selected);
                slideshowDatas.set(k, temp);
                imgSrcs.set(k-1, (Label)((HBox)selected.getChildren().get(0)).getChildren().get(2));
                imgSrcs.set(k, (Label)((HBox)temp.getChildren().get(0)).getChildren().get(2));
                imgCaptions.set(k-1, (TextField)((HBox)selected.getChildren().get(1)).getChildren().get(1));
                imgCaptions.set(k, (TextField)((HBox)temp.getChildren().get(1)).getChildren().get(1));
                GridPane.setRowIndex(selected, k-1);
                GridPane.setRowIndex(temp, k);
            }
        });
        moveDownSlide.setOnAction(u->{
            int k = GridPane.getRowIndex(selected);
            if(k< i-1){
                VBox temp = slideshowDatas.get(k+1);
                slideshowDatas.set(k+1, selected);
                slideshowDatas.set(k, temp);
                imgSrcs.set(k+1, (Label)((HBox)selected.getChildren().get(0)).getChildren().get(2));
                imgSrcs.set(k, (Label)((HBox)temp.getChildren().get(0)).getChildren().get(2));
                imgCaptions.set(k+1, (TextField)((HBox)selected.getChildren().get(1)).getChildren().get(1));
                imgCaptions.set(k, (TextField)((HBox)temp.getChildren().get(1)).getChildren().get(1));
                GridPane.setRowIndex(selected, k+1);
                GridPane.setRowIndex(temp, k);
            }
        });
        
        slideshowComponentDialog.getDialogPane().setContent(dialogBox);
        Optional<ButtonType>result = slideshowComponentDialog.showAndWait();
        if(result.isPresent() &&((result.get() != ButtonType.CANCEL))){
            i = 0;
            for(Label img : imgSrcs){
                EportfolioSlide slideToAdd = new EportfolioSlide(img.getText(), imgCaptions.get(i).getText());
                i++;
                srcs.add(slideToAdd);
            }
            return true;
        }
        return false;
    }

    public void editSlideshowComponentManager() {
        i = 0;
        Dialog editSlideshowComponentDialog = new Dialog();
        editSlideshowComponentDialog.setTitle("Slideshow Component Manager");
        ButtonType buttonYes = new ButtonType("Yes");
        editSlideshowComponentDialog.getDialogPane().getButtonTypes().addAll(buttonYes, ButtonType.CANCEL);
        editSlideshowComponentDialog.setResizable(true);
        
        HBox dialogBox = new HBox();
        VBox slideshowControlBox = new VBox();
        Button addSlide = new Button("Add Slide");
        Button removeSlide = new Button("Remove Slide");
        Button moveUpSlide = new Button ("Move Slide up");
        Button moveDownSlide = new Button("Move Slide down");
        slideshowControlBox.getChildren().addAll(addSlide, removeSlide, moveUpSlide, moveDownSlide);
        
        GridPane slidePane = new GridPane();
        ScrollPane slideScrollPane = new ScrollPane(slidePane);
        imgSrcs.clear();
        imgCaptions.clear();
        slideshowDatas.clear();
        for(EportfolioSlide slide :srcs){
            VBox slideDataComboBox = new VBox();
            HBox slideDataBox = new HBox();
            HBox captionDataBox = new HBox();
            Label imageSelectionLabel = new Label("Select Image: ");
            Button imageSelectionButton = new Button("Image");
            Label imageSrcLabel = new Label(slide.getSrc());
            imgSrcs.add(imageSrcLabel);
            Label imageCaptionLabel = new Label("Enter Image Caption: ");
            TextField imageCaptionTextField = new TextField(slide.getCaption());
            imgCaptions.add(imageCaptionTextField);
            CheckBox checkBox = new CheckBox();
            checkBox.setOnAction(x->{
                if(checkBox.isSelected()){
                    if(selected ==null){
                        selected = (VBox)checkBox.getParent().getParent();
                        if(GridPane.getRowIndex(selected) ==0){
                            moveUpSlide.setDisable(true);
                            moveDownSlide.setDisable(false);
                        }
                        else if(GridPane.getRowIndex(selected) == i-1){
                            moveDownSlide.setDisable(true);
                            moveUpSlide.setDisable(false);
                        }
                        else{
                            moveUpSlide.setDisable(false);
                            moveDownSlide.setDisable(false);
                        }
                    }
                    else if(selected !=null){
                       // ((CheckBox)selected.getChildren().get(0))
                        HBox temp = (HBox)selected.getChildren().get(0);
                        CheckBox che = (CheckBox)temp.getChildren().get(3);
                        che.setSelected(false);
                        selected = (VBox)checkBox.getParent().getParent();
                                                if(GridPane.getRowIndex(selected) ==0){
                            moveUpSlide.setDisable(true);
                        }
                        else{
                            moveUpSlide.setDisable(false);
                        }
                    }
                }
                else if(!(checkBox.isSelected())){
                    selected = null;
                }
            });
            
            slideDataBox.getChildren().addAll(imageSelectionLabel, imageSelectionButton, imageSrcLabel, checkBox);
            captionDataBox.getChildren().addAll(imageCaptionLabel, imageCaptionTextField);
            slideDataComboBox.getChildren().addAll(slideDataBox, captionDataBox);
            slideshowDatas.add(slideDataComboBox);
            slidePane.add(slideDataComboBox, 0, i);
            i++;
            imageSelectionButton.setOnAction(f->{
                FileChooser fileChooser = new FileChooser();
                fileChooser.setTitle("Choice Image");
                fileChooser.setInitialDirectory(new File(System.getProperty("user.home")));
        
              fileChooser.getExtensionFilters().addAll(
                    new FileChooser.ExtensionFilter("JPG", "*.jpg"),
                    new FileChooser.ExtensionFilter("PNG", "*.png")
                );
                img = fileChooser.showOpenDialog(null);
                if(img != null){
                    imageSrcLabel.setText(img.getPath());
                }     
            });
        }
        
        dialogBox.getChildren().add(slideshowControlBox);
        dialogBox.getChildren().add(slideScrollPane);
        addSlide.setOnAction(e->{
            editSlideshowComponentDialog.setHeight(300);
            editSlideshowComponentDialog.setWidth(800);
            VBox slideDataComboBox = new VBox();
            HBox slideDataBox = new HBox();
            HBox captionDataBox = new HBox();
            Label imageSelectionLabel = new Label("Select Image: ");
            Button imageSelectionButton = new Button("Image");
            Label imageSrcLabel = new Label();
            Label imageCaptionLabel = new Label("Enter Image Caption: ");
            TextField imageCaptionTextField = new TextField();
            imgCaptions.add(imageCaptionTextField);
            CheckBox checkBox = new CheckBox();
            checkBox.setOnAction(x->{
                if(checkBox.isSelected()){
                    if(selected ==null){
                        selected = (VBox)checkBox.getParent().getParent();
                        if(GridPane.getRowIndex(selected) ==0){
                            moveUpSlide.setDisable(true);
                            moveDownSlide.setDisable(false);
                        }
                        else if(GridPane.getRowIndex(selected) == i-1){
                            moveDownSlide.setDisable(true);
                            moveUpSlide.setDisable(false);
                        }
                        else{
                            moveUpSlide.setDisable(false);
                            moveDownSlide.setDisable(false);
                        }
                    }
                    else if(selected !=null){
                       // ((CheckBox)selected.getChildren().get(0))
                        HBox temp = (HBox)selected.getChildren().get(0);
                        CheckBox che = (CheckBox)temp.getChildren().get(3);
                        che.setSelected(false);
                        selected = (VBox)checkBox.getParent().getParent();
                                                if(GridPane.getRowIndex(selected) ==0){
                            moveUpSlide.setDisable(true);
                        }
                        else{
                            moveUpSlide.setDisable(false);
                        }
                    }
                }
                else if(!(checkBox.isSelected())){
                    selected = null;
                }
            });
            slideDataBox.getChildren().addAll(imageSelectionLabel, imageSelectionButton, imageSrcLabel, checkBox);
            captionDataBox.getChildren().addAll(imageCaptionLabel, imageCaptionTextField);
            slideDataComboBox.getChildren().addAll(slideDataBox, captionDataBox);
            slideshowDatas.add(slideDataComboBox);
            slidePane.add(slideDataComboBox, 0, i);
            i++;
            imageSelectionButton.setOnAction(f->{
                FileChooser fileChooser = new FileChooser();
                fileChooser.setTitle("Choice Image");
                fileChooser.setInitialDirectory(new File(System.getProperty("user.home")));
        
              fileChooser.getExtensionFilters().addAll(
                    new FileChooser.ExtensionFilter("JPG", "*.jpg"),
                    new FileChooser.ExtensionFilter("PNG", "*.png")
                );
                img = fileChooser.showOpenDialog(null);
                if(img != null){
                    imageSrcLabel.setText(img.getPath());
                    imgSrcs.add(imageSrcLabel);

                }     
            });

        });
        removeSlide.setOnAction(r->{
            slidePane.getChildren().remove(selected);
            imgSrcs.remove((Label)((HBox)selected.getChildren().get(0)).getChildren().get(2));
            imgCaptions.remove((TextField)((HBox)selected.getChildren().get(1)).getChildren().get(1));
            selected = null;
        });
        moveUpSlide.setOnAction(u->{
            int k = GridPane.getRowIndex(selected);
            if(k> 0){
                VBox temp = slideshowDatas.get(k-1);
                slideshowDatas.set(k-1, selected);
                slideshowDatas.set(k, temp);
                imgSrcs.set(k-1, (Label)((HBox)selected.getChildren().get(0)).getChildren().get(2));
                imgSrcs.set(k, (Label)((HBox)temp.getChildren().get(0)).getChildren().get(2));
                imgCaptions.set(k-1, (TextField)((HBox)selected.getChildren().get(1)).getChildren().get(1));
                imgCaptions.set(k, (TextField)((HBox)temp.getChildren().get(1)).getChildren().get(1));
                GridPane.setRowIndex(selected, k-1);
                GridPane.setRowIndex(temp, k);
            }
        });
        moveDownSlide.setOnAction(u->{
            int k = GridPane.getRowIndex(selected);
            if(k< i-1){
                VBox temp = slideshowDatas.get(k+1);
                slideshowDatas.set(k+1, selected);
                slideshowDatas.set(k, temp);
                imgSrcs.set(k+1, (Label)((HBox)selected.getChildren().get(0)).getChildren().get(2));
                imgSrcs.set(k, (Label)((HBox)temp.getChildren().get(0)).getChildren().get(2));
                imgCaptions.set(k+1, (TextField)((HBox)selected.getChildren().get(1)).getChildren().get(1));
                imgCaptions.set(k, (TextField)((HBox)temp.getChildren().get(1)).getChildren().get(1));
                GridPane.setRowIndex(selected, k+1);
                GridPane.setRowIndex(temp, k);
            }
        });
        
        editSlideshowComponentDialog.getDialogPane().setContent(dialogBox);
        Optional<ButtonType>result = editSlideshowComponentDialog.showAndWait();
        if(result.isPresent() &&((result.get() != ButtonType.CANCEL))){
            srcs.clear();
            i = 0;
            for(Label img : imgSrcs){
                EportfolioSlide slideToAdd = new EportfolioSlide(img.getText(), imgCaptions.get(i).getText());
                i++;
                srcs.add(slideToAdd);
            }
        }
    }
    
}
