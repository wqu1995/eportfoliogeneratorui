/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epg.model;

import java.io.File;
import java.util.Optional;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.stage.FileChooser;

/**
 *
 * @author wqu
 */
public class EportfolioVideoComponent extends EportfolioComponent{
    String videoSrc;
    String caption;
    int width=100;
    int height=100;
    public EportfolioVideoComponent(String initType) {
        super(initType);
    }
    
    public void setVideoSrc(String initSrc){
        videoSrc = initSrc;
    }
    public void setVideoCaption(String initCaption){
        caption = initCaption;
    }
    public void setVideoWidth(int initWidth){
        width = initWidth;
    }
    public void setVideoHeight(int initHeight){
        height = initHeight;
    }
    public String getVideoSrc (){
        return videoSrc;
    }
    public String getCaption(){
        return caption;
    }
    public int getWidth(){
        return width;
    }
    public int getHeight(){
        return height;
    }
    public boolean manageVideoComponent() {
        Dialog videoComponentDialog = new Dialog();
        videoComponentDialog.setTitle("Video Component Manager");
        ButtonType buttonYes = new ButtonType("Yes");
        videoComponentDialog.getDialogPane().getButtonTypes().addAll(buttonYes, ButtonType.CANCEL);
        videoComponentDialog.setResizable(true);
        
        GridPane grid = new GridPane();
        Label videoSelectionlabel = new Label("Choice Video: ");
        Button videoSelectionButton = new Button();
        if(videoSrc == null){
            videoSelectionButton.setText("video");
        }
        else{
            videoSelectionButton.setText(videoSrc);
        }
        Label videoCaptionLabel = new Label ("Enter video Caption: ");
        TextField videoCaptionTextField = new TextField(caption);
        HBox widthBox = new HBox();
        Label widthLabel = new Label("Enter video widht in %");
        TextField widthTextField = new TextField(Integer.toString(width));
        widthBox.getChildren().addAll(widthLabel, widthTextField);
        HBox heightBox = new HBox();
        Label heightLabel = new Label("Enter video Height in %");
        TextField heightTextField = new TextField(Integer.toString(height));
        heightBox.getChildren().addAll(heightLabel, heightTextField);
        
        videoSelectionButton.setOnAction(e->{
            File video;
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Choice Video");
            fileChooser.setInitialDirectory(new File(System.getProperty("user.home")));
        
            fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("MP4", "*.mp4"),
                new FileChooser.ExtensionFilter("OGG", "*.ogg")
            );
            video = fileChooser.showOpenDialog(null);
            if(video != null){
                 videoSrc = video.getPath();
                 videoSelectionButton.setText(video.getPath());
            }  
        });
        
        grid.add(videoSelectionlabel, 0, 0);
        grid.add(videoSelectionButton, 1, 0);
        grid.add(videoCaptionLabel, 0, 1);
        grid.add(videoCaptionTextField, 1, 1);
        grid.add(widthBox, 0, 2);
        grid.add(heightBox, 1, 2);

        videoComponentDialog.getDialogPane().setContent(grid);
        Optional<ButtonType>result = videoComponentDialog.showAndWait();
        if(result.isPresent() &&((result.get() != ButtonType.CANCEL))){
            caption = videoCaptionTextField.getText();
            width = Integer.parseInt(widthTextField.getText());
            height = Integer.parseInt(heightTextField.getText());
            return true;
        }
        return false;
    }

    
}
