/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epg.model;

import epg.view.EportfolioGeneratorView;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import properties_manager.PropertiesManager;

/**
 *
 * @author Wenjun
 */
public class EportfolioPageModel {
    EportfolioGeneratorView ui;
    ObservableList<EportfolioPage> pages;
    EportfolioPage selectedPage;
    
    public EportfolioPageModel(EportfolioGeneratorView initUI){
        ui = initUI;
        pages = FXCollections.observableArrayList();
        rest();
    }
    public void addPage(String initPageTitle) {
        EportfolioPage pageToAdd = new EportfolioPage(initPageTitle);
        pages.add(pageToAdd);
        ui.reloadPagePane(this);
    }

    private void rest() {
    }

    public ObservableList<EportfolioPage> getPages() {
        return pages;
    }

    public void setSelected(EportfolioPage initPage) {
        selectedPage = initPage;
        selectedPage.selected = true;
    }

    public EportfolioPage getSelectedPage() {
        return selectedPage;
    }


    public void removeSelectedPage() {
        pages.remove(selectedPage);
        selectedPage = null;
        ui.reloadPagePane(this);
        ui.reloadPageWorkSpace(selectedPage);
        ui.updatePageControlToolBar(this);
    }

    public void reset() {
        pages.clear();
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        selectedPage = null;
        EportfolioPage.navigation.clear();
    }
    
}
