/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epg.model;

import epg.view.EportfolioWorkspace;
import java.util.ArrayList;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Dialog;
import javafx.scene.layout.GridPane;
import javax.json.JsonValue;

/**
 *
 * @author Wenjun
 */
public class EportfolioPage {
    String pageTitle;
    boolean selected;
    ObservableList<EportfolioComponent> components;
    EportfolioComponent selectedComponent;
    
    static String studentName = "";
    String footerMsg = "This Website is Generated and Maintained by ePortfolio Generator";
    String bannerImgPath = "null";
    String pageTemplate = "Navigation_left";
    String colorTemplate = "Blue";
    String fontTemplate = "Comfortaa";
    private EportfolioWorkspace workspaceUI;
    static ArrayList<String> navigation = new ArrayList();
    
    EportfolioPage(String initPageTitle) {
        //workspaceUI = initWorksapceUI;
        pageTitle = initPageTitle;
        components = FXCollections.observableArrayList();
        navigation.add(pageTitle);
    }
    public ArrayList<String> getNavigation(){
        return navigation;
    }
    public void setTitle(String newValue) {
        pageTitle = newValue;
    }
    public String getTitle(){
        return pageTitle;
    }
    public ObservableList<EportfolioComponent> getComponents(){
        return components;
    }
    public EportfolioComponent getSelectedComponent(){
        return selectedComponent;
    }

    public void setStudentName(String newValue) {
        studentName = newValue;
    }
    public String getStudentName(){
        return studentName;
    }


    public void setBannerImgSrc(String path) {
        bannerImgPath = path;
    }

    public String getBannerImgSrc() {
        return bannerImgPath;
    }

    public void setTemplate(String value) {
        pageTemplate = value;
    }
    public String getTemplate(){
        return pageTemplate;
    }
    public void setColor(String value) {
        colorTemplate = value;
    }
    public String getColor(){
        return colorTemplate;
    }

    public void setFont(String value) {
        fontTemplate = value;
    }
    public String getFont(){
        return fontTemplate;
    }
    public void setFooter(String value){
        footerMsg = value;
    }
    public String getFooter() {
        return footerMsg;
    }
    public boolean isSelected(){
        if(selectedComponent != null){
            return true;
        }
        else{
            return false;
        }
    }
    public void addComponent(String componentType) {
        boolean result;
        if(componentType == "text"){
            EportfolioTextComponent componentToAdd = new EportfolioTextComponent(componentType);
                     result = componentToAdd.manageTextComponent();
                    if(result)
                        components.add(componentToAdd);

        }
        else if(componentType =="image"){
            EportfolioImageComponent componentToAdd = new EportfolioImageComponent(componentType);
                    result = componentToAdd.manageImageComponent();
                    if(result)
                        components.add(componentToAdd);

        }
        else if(componentType =="video"){
            EportfolioVideoComponent componentToAdd = new EportfolioVideoComponent(componentType);
            result = componentToAdd.manageVideoComponent();        
            if(result)
                components.add(componentToAdd);

        }
        else if(componentType =="slideshow"){
            EportfolioSlideshowComponent componentToAdd = new EportfolioSlideshowComponent(componentType);
                    
            result = componentToAdd.manageSlideshowComponent();

                    if(result)
                        components.add(componentToAdd);

        }
    }

    public void setSelectedComponent(EportfolioComponent component) {
        selectedComponent = component;
    }

    public void editComponent() {
        if(selectedComponent.getType().equals("text")){
            ((EportfolioTextComponent)selectedComponent).edtTextComponentManager();
        }
        else if(selectedComponent.getType().equals("image")){
            ((EportfolioImageComponent)selectedComponent).manageImageComponent();
        }
        else if(selectedComponent.getType().equals("video")){
            ((EportfolioVideoComponent)selectedComponent).manageVideoComponent();
        }
        else if(selectedComponent.getType().equals("slideshow")){
            ((EportfolioSlideshowComponent)selectedComponent).editSlideshowComponentManager();
        }
    }

    public void removeComponent() {
        components.remove(selectedComponent);
        selectedComponent = null;
    }

    public String getName() {
        return studentName;
    }


}
