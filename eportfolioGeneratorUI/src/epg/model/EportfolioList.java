/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epg.model;

import java.util.ArrayList;

/**
 *
 * @author wqu
 */
public class EportfolioList {
    ArrayList<String> srcs;
    ArrayList<String> hyperlinkSrcs;
    ArrayList<String> hyperlinkDists;
    boolean hyperlink;
    public EportfolioList(){
        srcs = new ArrayList();
        hyperlinkSrcs = new ArrayList();
        hyperlinkDists = new ArrayList();

    }
    public EportfolioList(String src){
        srcs = new ArrayList();
        hyperlinkSrcs = new ArrayList();
        hyperlinkDists = new ArrayList();

        srcs.add(src);
    }
    
    public ArrayList<String> getSrcs(){
        return srcs;
    }
    public ArrayList<String> getHyperlinkSrcs(){
        return hyperlinkSrcs;
    }
    public ArrayList<String> getHyperlinkDists(){
        return hyperlinkDists;
    }
    public void setHyperlink(boolean value){
        hyperlink = value;
    }

}
