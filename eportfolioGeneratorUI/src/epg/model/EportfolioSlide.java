/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epg.model;

/**
 *
 * @author wqu
 */
public class EportfolioSlide {
    String imgSrc;
    String caption;

    public EportfolioSlide(String path, String text) {
        imgSrc = path;
        caption = text;
    }
    public String getSrc(){
        return imgSrc;
    }
    public String getCaption(){
        return caption;
    }
}
