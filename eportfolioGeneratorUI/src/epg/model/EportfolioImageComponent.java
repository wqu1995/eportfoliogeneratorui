/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epg.model;

import java.io.File;
import java.util.Optional;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.stage.FileChooser;

/**
 *
 * @author wqu
 */
public class EportfolioImageComponent extends EportfolioComponent{
    String imgSrc;
    String caption;
    int width=100;
    int height=100;
    String displayOption;
    public EportfolioImageComponent(String initType) {
        super(initType);
    }
    public void setImageSrc(String path){
        imgSrc = path;
    }
    public void setCaption(String initCaption){
        caption = initCaption;
    }
    public void setWidth(int initWidth){
        width = initWidth;
    }
    public void setHeight(int initHeight){
        width = initHeight;
    }
    public void setDisplayOption(String option){
        displayOption = option;
    }
    public String getImageSrc(){
        return imgSrc;
    }
    public String getCaption(){
        return caption;
    }    
    public int getWidth(){
        return width;
    }    
    public int getHiehgt(){
        return height;
    }
    public String getImageOption(){
        return displayOption;
    }
    public boolean manageImageComponent() {
        Dialog imageComponentDialog = new Dialog();
        imageComponentDialog.setTitle("Image Component Manager");
        ButtonType buttonYes = new ButtonType("Yes");
        imageComponentDialog.getDialogPane().getButtonTypes().addAll(buttonYes, ButtonType.CANCEL);
        imageComponentDialog.setResizable(true);
        
        GridPane grid = new GridPane();
        Label imageSelectionLabel = new Label("Choice Image: ");
        Button imageSelectionButton = new Button();
        if(imgSrc == null){
            imageSelectionButton.setText("Image");
        }
        else{
            imageSelectionButton.setText(imgSrc);
        }
        Label imageCaptionLabel = new Label("Enter image Caption: ");
        TextField imageCaptionTextField = new TextField(caption);
        HBox widthBox = new HBox();
        Label widthLabel = new Label("Enter image widht in %");
        TextField widthTextField = new TextField(Integer.toString(width));
        widthBox.getChildren().addAll(widthLabel, widthTextField);
        HBox heightBox = new HBox();
        Label heightLabel = new Label("Enter image Height in %");
        TextField heightTextField = new TextField(Integer.toString(height));
        heightBox.getChildren().addAll(heightLabel, heightTextField);
        Label imageOptionLabel = new Label("Image Option: ");
        ComboBox imageOptionBox = new ComboBox();
        imageOptionBox.getItems().addAll("none","float left", "float right");
        if(displayOption == null){
            imageOptionBox.setValue("none");
        }
        else{
            imageOptionBox.setValue(displayOption);
        }
        imageSelectionButton.setOnAction(e->{
            File img;
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Choice Image");
            fileChooser.setInitialDirectory(new File(System.getProperty("user.home")));
        
            fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("JPG", "*.jpg"),
                new FileChooser.ExtensionFilter("PNG", "*.png")
            );
            img = fileChooser.showOpenDialog(null);
            if(img != null){
                 imgSrc = img.getPath();
                 imageSelectionButton.setText(img.getPath());
            }        
        });
        
        grid.add(imageSelectionLabel, 0, 0);
        grid.add(imageSelectionButton, 1, 0);
        grid.add(imageCaptionLabel, 0, 1);
        grid.add(imageCaptionTextField, 1, 1);
        grid.add(widthBox, 0, 2);
        grid.add(heightBox, 1, 2);
        grid.add(imageOptionLabel, 0, 3);
        grid.add(imageOptionBox, 1, 3);
        
        imageComponentDialog.getDialogPane().setContent(grid);
        Optional<ButtonType>result = imageComponentDialog.showAndWait();
        if(result.isPresent() &&(result.get() != ButtonType.CANCEL)){
            caption = imageCaptionTextField.getText();
            width = Integer.parseInt(widthTextField.getText());
            height = Integer.parseInt(heightTextField.getText());
            displayOption = (String) imageOptionBox.getValue();
            return true;
        }
        return false;
    }
    
}
