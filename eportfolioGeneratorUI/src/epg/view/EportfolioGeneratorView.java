/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epg.view;

import epg.LanguagePropertyType;
import static epg.LanguagePropertyType.TOOLTIP_ADD_PAGE;
import static epg.LanguagePropertyType.TOOLTIP_EXIT;
import static epg.LanguagePropertyType.TOOLTIP_EXPORT_WORK;
import static epg.LanguagePropertyType.TOOLTIP_LOAD_WORK;
import static epg.LanguagePropertyType.TOOLTIP_NEW_WORK;
import static epg.LanguagePropertyType.TOOLTIP_PAGE_DOWN;
import static epg.LanguagePropertyType.TOOLTIP_PAGE_UP;
import static epg.LanguagePropertyType.TOOLTIP_REMOVE_PAGE;
import static epg.LanguagePropertyType.TOOLTIP_SAVE_AS_WORK;
import static epg.LanguagePropertyType.TOOLTIP_SAVE_WORK;
import static epg.StartupConstants.CSS_CLASS_MAINUI_FILE_TOOL_BAR;
import static epg.StartupConstants.CSS_CLASS_MAINUI_FILE_TOOL_BAR_BUTTON;
import static epg.StartupConstants.CSS_CLASS_PAGE_CONTROL_TOOL_BAR;
import static epg.StartupConstants.CSS_CLASS_PAGE_CONTROL_TOOL_BAR_BUTTON;
import static epg.StartupConstants.CSS_CLASS_PAGE_SCROLL_PANE;
import static epg.StartupConstants.CSS_CLASS_SELECTED;
import static epg.StartupConstants.CSS_CLASS_WORK_SPACE_PANE;
import static epg.StartupConstants.CSS_CLASS_WORK_TITLE_VBOX;
import static epg.StartupConstants.ICON_ADD_PAGE;
import static epg.StartupConstants.ICON_EXIT;
import static epg.StartupConstants.ICON_EXPORT_WORK;
import static epg.StartupConstants.ICON_LOAD_WORK;
import static epg.StartupConstants.ICON_NEW_WORK;
import static epg.StartupConstants.ICON_PAGE_DOWN;
import static epg.StartupConstants.ICON_PAGE_UP;
import static epg.StartupConstants.ICON_REMOVE_PAGE;
import static epg.StartupConstants.ICON_SAVE_AS_WORK;
import static epg.StartupConstants.ICON_SAVE_WORK;
import static epg.StartupConstants.PATH_ICON;
import static epg.StartupConstants.WINDOW_ICON;
import epg.error.ErrorHandler;
import epg.file.EportfolioGeneratorFileManager;

import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.stage.Screen;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;
import static epg.StartupConstants.PATH_ICON;
import static epg.StartupConstants.PATH_WEB_VIEW;
import static epg.StartupConstants.STYLE_SHEET_UI;
import static epg.StartupConstants.WINDOW_ICON;
import epg.controller.EportfolioGeneratorFileController;
import epg.controller.EportfolioGeneratorPageController;
import epg.file.EportfolioDataGenerator;
import epg.file.EportfolioWebJsonGenerator;
import epg.model.EportfolioPage;
import epg.model.EportfolioPageModel;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

/**
 *
 * @author wqu
 */
public class EportfolioGeneratorView {
    
    Stage primaryStage;
    Scene PrimaryScene;
    
    FlowPane fileToolbarPane;
    Button newWorkButton;
    Button loadWorkButton;
    Button saveWorkButton;
    Button saveAsWorkButton;
    Button exportWorkButton;
    Button exitWorkButton;
    
    BorderPane ePortfolioPane;
    TextField titleTextField;
    
    HBox workspace;
    
    VBox pageControlToolbar;
    Button addPageButton;
    Button removePageButton;
    Button pageUpButton;
    Button pageDownButton;
    
    VBox pagePane;
    ScrollPane pageScrollPane;
    
    TabPane GenerartorModePane;
    Tab componentWorkSpace;
    Tab pageViewWorkSpace;
    
    EportfolioPageModel pageModel;
    EportfolioGeneratorFileManager fileManager;
    private ErrorHandler errorHandler;
    private EportfolioGeneratorFileController fileController;
    private EportfolioGeneratorPageController pageController;


    public EportfolioGeneratorView(EportfolioGeneratorFileManager initFileManager) {
        fileManager = initFileManager;
        errorHandler = new ErrorHandler(this);
        pageModel = new EportfolioPageModel(this);
    }

    public void startUI(Stage initPrimaryStage, String appTitle) {
        initFileToolbar();
        initWorkspace();
        initEventHandlers();
        
        primaryStage = initPrimaryStage;
        primaryStage.getIcons().add(new Image("file:" +PATH_ICON + WINDOW_ICON));
        initWindow(appTitle);
    }

    private void initWindow(String appTitle) {
        primaryStage.setTitle(appTitle);
        
        Screen screen = Screen.getPrimary();
        Rectangle2D bounds = screen. getVisualBounds();
        
        primaryStage.setX(bounds.getMinX());
	primaryStage.setY(bounds.getMinY());
	primaryStage.setWidth(bounds.getWidth());
	primaryStage.setHeight(bounds.getHeight());
        
        ePortfolioPane = new BorderPane();
        ePortfolioPane.setTop(fileToolbarPane);
        PrimaryScene = new Scene(ePortfolioPane);
        PrimaryScene.getStylesheets().add(STYLE_SHEET_UI);
        
        primaryStage.setScene(PrimaryScene);
        primaryStage.show();
        
    }

    public ErrorHandler getErrorHandler() {
        return errorHandler;
    }

    private void initFileToolbar() {
        fileToolbarPane = new FlowPane();
        fileToolbarPane.getStyleClass().add(CSS_CLASS_MAINUI_FILE_TOOL_BAR);
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        newWorkButton = initChildButton(fileToolbarPane, ICON_NEW_WORK, TOOLTIP_NEW_WORK, CSS_CLASS_MAINUI_FILE_TOOL_BAR_BUTTON, false);
        saveWorkButton = initChildButton(fileToolbarPane, ICON_SAVE_WORK, TOOLTIP_SAVE_WORK, CSS_CLASS_MAINUI_FILE_TOOL_BAR_BUTTON, true);
        saveAsWorkButton = initChildButton(fileToolbarPane, ICON_SAVE_AS_WORK, TOOLTIP_SAVE_AS_WORK, CSS_CLASS_MAINUI_FILE_TOOL_BAR_BUTTON, true);
        loadWorkButton = initChildButton(fileToolbarPane, ICON_LOAD_WORK, TOOLTIP_LOAD_WORK, CSS_CLASS_MAINUI_FILE_TOOL_BAR_BUTTON, false);
        exportWorkButton = initChildButton(fileToolbarPane, ICON_EXPORT_WORK, TOOLTIP_EXPORT_WORK, CSS_CLASS_MAINUI_FILE_TOOL_BAR_BUTTON, true);
        exitWorkButton = initChildButton(fileToolbarPane, ICON_EXIT, TOOLTIP_EXIT, CSS_CLASS_MAINUI_FILE_TOOL_BAR_BUTTON, false);
    }

    public Button initChildButton(
            Pane toolbar, 
	    String iconFileName, 
	    LanguagePropertyType tooltip, 
	    String cssClass,
	    boolean disabled) {
	PropertiesManager props = PropertiesManager.getPropertiesManager();
	String imagePath = "file:" + PATH_ICON + iconFileName;
	Image buttonImage = new Image(imagePath);
	javafx.scene.control.Button button = new javafx.scene.control.Button();
	button.getStyleClass().add(cssClass);
	button.setDisable(disabled);
	button.setGraphic(new ImageView(buttonImage));
	Tooltip buttonTooltip = new Tooltip(props.getProperty(tooltip.toString()));
	button.setTooltip(buttonTooltip);
	toolbar.getChildren().add(button);
	return button;
    }

    private void initWorkspace() {
        workspace = new HBox();
       
        GenerartorModePane= new TabPane();
        GenerartorModePane.setOnMouseClicked(e->{
            EportfolioWebJsonGenerator jsonGenerator = new EportfolioWebJsonGenerator();
            try {
                jsonGenerator.generateWebViewJson(pageModel.getSelectedPage(), PATH_WEB_VIEW+"data/sample_view.json");
                EportfolioDataGenerator dataGenerator = new EportfolioDataGenerator(pageModel.getSelectedPage());
            } catch (FileNotFoundException ex) {
                Logger.getLogger(EportfolioGeneratorView.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(EportfolioGeneratorView.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
        GenerartorModePane.getStyleClass().add(CSS_CLASS_WORK_SPACE_PANE);
        GenerartorModePane.setTabClosingPolicy(TabPane.TabClosingPolicy.UNAVAILABLE);

        componentWorkSpace = new Tab();
        componentWorkSpace.setText("work space");
        GenerartorModePane.getTabs().add(componentWorkSpace);
        
        pageViewWorkSpace = new Tab();
        pageViewWorkSpace.setText("view space");
        
        GenerartorModePane.getTabs().add(pageViewWorkSpace);        

        pageControlToolbar = new VBox();
        pageControlToolbar.getStyleClass().add(CSS_CLASS_MAINUI_FILE_TOOL_BAR);
        addPageButton = initChildButton(pageControlToolbar, ICON_ADD_PAGE, TOOLTIP_ADD_PAGE, CSS_CLASS_PAGE_CONTROL_TOOL_BAR_BUTTON, false);
        removePageButton = initChildButton(pageControlToolbar, ICON_REMOVE_PAGE, TOOLTIP_REMOVE_PAGE, CSS_CLASS_PAGE_CONTROL_TOOL_BAR_BUTTON, true);
        
        pagePane = new VBox();
        pageScrollPane = new ScrollPane(pagePane);
        pageScrollPane.getStyleClass().add(CSS_CLASS_PAGE_SCROLL_PANE);
        
        workspace.getChildren().add(pageControlToolbar);
        workspace.getChildren().add(pageScrollPane);
        HBox.setHgrow(GenerartorModePane, Priority.ALWAYS);
        workspace.getChildren().add(GenerartorModePane);

    }

    public void initEventHandlers() {
        fileController = new EportfolioGeneratorFileController(this, fileManager);
        
        newWorkButton.setOnAction(e->{
            fileController.handleNewWorkRequest();
        });
        saveWorkButton.setOnAction(e->{
            fileController.handleSaveWorkRequest();
        });
        saveAsWorkButton.setOnAction(e->{
            fileController.handleSaveAsWorkRequest();
        });
        loadWorkButton.setOnAction(e->{
            fileController.handleLoadWorkRequest();
        });
        exportWorkButton.setOnAction(e->{
            fileController.handleExportWorkRequest();
        });
        exitWorkButton.setOnAction(e->{
            fileController.handleExitWorkRequest();
        });
        
        pageController = new EportfolioGeneratorPageController(this);
        addPageButton.setOnAction(e->{
            pageController.handleAddPageRequest();
                        fileController.markAsEdited();

        });
        removePageButton.setOnAction(e->{
            pageController.handleRemovePageRequest();
                        fileController.markAsEdited();

        });
    }

    public void updateToolbarControls(boolean saved) {
        ePortfolioPane.setCenter(workspace);
        if(pageModel.getPages().size()!=0){
            saveAsWorkButton.setDisable(false);
            saveWorkButton.setDisable(saved);
            exportWorkButton.setDisable(false);
        }
        else if(pageModel.getPages().size()==0){
            saveAsWorkButton.setDisable(saved);
            saveWorkButton.setDisable(saved);
            exportWorkButton.setDisable(true);
        }
        
    }

    public EportfolioPageModel getPages() {
        return pageModel;
    }
    
    public void reloadPagePane(EportfolioPageModel pageToLoad){
        pagePane.getChildren().clear();

        Label pageListLabel = new Label("Page List: ");
        pageListLabel.getStyleClass().add(CSS_CLASS_WORK_TITLE_VBOX);
        pagePane.getChildren().add(pageListLabel);
        int pageIndex=0;
        for(EportfolioPage page: pageToLoad.getPages()){
            
            EportfolioPageEditView pageEditView = new EportfolioPageEditView(page, fileController ,pageIndex);
  
            if(!page.getTitle().equals("test")){
                pageEditView.setTitleText(page.getTitle());
            }
            pageEditView.setOnMouseClicked(e->{
                pageController.processSelectedPage(pageToLoad, page, this);
            });
            
            if(page == pageToLoad.getSelectedPage()){
                pageEditView.getStyleClass().add(CSS_CLASS_SELECTED);
            }
            pagePane.getChildren().add(pageEditView);
            pageIndex++;
        }
        //updateToolbarControls(false);
    }

    public void reloadPageWorkSpace(EportfolioPage page){
        if (page!=null){
                EportfolioWorkspace workspaceView = new EportfolioWorkspace(page, fileController);
                componentWorkSpace.setContent(workspaceView);

        }
        else if(page ==null){
            EportfolioWorkspace workspaceView = new EportfolioWorkspace(false);
            componentWorkSpace.setContent(workspaceView);
        }
               // updateToolbarControls(false);

    }

    public void reloadWebViewSpace(EportfolioPage page) {
        pageViewWorkSpace.setContent(new EportfolioWebViewSpace(page));
    }

    public void updatePageControlToolBar(EportfolioPageModel pageModel) {
        if(pageModel.getSelectedPage() !=null){
            removePageButton.setDisable(false);
        }
    }

}
