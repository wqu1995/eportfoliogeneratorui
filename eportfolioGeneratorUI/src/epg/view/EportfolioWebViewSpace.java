/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epg.view;

import static epg.StartupConstants.PATH_WEB_VIEW;
import epg.file.EportfolioGeneratorFileManager;
import epg.file.EportfolioWebJsonGenerator;
import epg.model.EportfolioComponent;
import epg.model.EportfolioImageComponent;
import epg.model.EportfolioPage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.geometry.HPos;
import static javafx.geometry.Pos.CENTER;
import javafx.geometry.VPos;
import javafx.scene.layout.HBox;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;

/**
 *
 * @author Wenjun
 */
public class EportfolioWebViewSpace extends HBox {
      public static String SLASH = "/";
    WebView browser = new WebView();
    WebEngine webEngine = browser.getEngine();
    File pageDir;
    File dataDir;
    File imgDir;
    File videoDir;
    EportfolioPage page;
    public EportfolioWebViewSpace(EportfolioPage initPage) {
              page = initPage;
              File index = new File(PATH_WEB_VIEW + "sample_view.html");
              
              webEngine.load(index.toURI().toString());
              webEngine.setJavaScriptEnabled(true);
              getChildren().add(browser);

    }

    @Override
    protected void layoutChildren(){
        double w = getWidth();
        double h = getHeight();
        layoutInArea(browser,0,0,w,h,0, HPos.CENTER, VPos.CENTER);
    }

}
