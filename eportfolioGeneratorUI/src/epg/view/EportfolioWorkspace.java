/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epg.view;

import epg.LanguagePropertyType;
import static epg.LanguagePropertyType.TOOLTIP_ADD_IMAGE_COMPONENT;
import static epg.LanguagePropertyType.TOOLTIP_ADD_SLIDESHOW_COMPONENT;
import static epg.LanguagePropertyType.TOOLTIP_ADD_TEXT_COMPONENT;
import static epg.LanguagePropertyType.TOOLTIP_ADD_VIDEO_COMPONENT;
import static epg.LanguagePropertyType.TOOLTIP_EDIT_COMPONENT;
import static epg.LanguagePropertyType.TOOLTIP_EDIT_HYPERLINK;
import static epg.LanguagePropertyType.TOOLTIP_REMOVE_COMPONENT;
import static epg.StartupConstants.CSS_CLASS_COMPONENT_SLELECTED;
import static epg.StartupConstants.CSS_CLASS_WORKSPACE_BUTTON;
import static epg.StartupConstants.CSS_CLASS_WORKSPACE_CONTROL_TOOL_BAR;
import static epg.StartupConstants.CSS_CLASS_WORKSPACE_CONTROL_TOOL_BAR_BUTTON;
import static epg.StartupConstants.CSS_CLASS_WORKSPACE_EMPTY_PANE;
import static epg.StartupConstants.CSS_CLASS_WORK_SPACE_COMPONENT_PADDING;
import static epg.StartupConstants.CSS_CLASS_WORK_SPACE_PAGE_DATA_PANE;
import static epg.StartupConstants.ICON_ADD_IMAGE_COMPONENT;
import static epg.StartupConstants.ICON_ADD_SLIDESHOW_COMPONENT;
import static epg.StartupConstants.ICON_ADD_TEXT_COMPONENT;
import static epg.StartupConstants.ICON_ADD_VIDEO_COMPONENT;
import static epg.StartupConstants.ICON_EDIT_COMPONENT;
import static epg.StartupConstants.ICON_EDIT_HYPERLINK;
import static epg.StartupConstants.ICON_REMOVE_COMPONENT;
import static epg.StartupConstants.PATH_ICON;
import epg.controller.EportfolioGeneratorFileController;
import epg.controller.EportfolioWorkspaceController;
import epg.model.EportfolioComponent;
import epg.model.EportfolioPage;
import epg.model.EportfolioTextComponent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import properties_manager.PropertiesManager;

/**
 *
 * @author Wenjun
 */
public class EportfolioWorkspace extends HBox{
    int i = 0;
    EportfolioPage page;
    VBox workspaceToolBar;
    Button addTextComponentButton;
    Button addImageComponentButton;
    Button addSlideshowComponentButton;
    Button addVideoComponentButton;
    Button editComponentButton;
    Button editHyperlinkButton;
    Button removeComponentButton;
    
    ScrollPane workspacePane;
    GridPane pageDataPane;
    GridPane personalDataPane;
    GridPane componentDataPane;
    
    HBox personDataBox;
    Label bannerImageLabel;
    Button setBannerImageButton;
    Label studentNameLabel;
    TextField studentNameTextField;
    
    HBox footerBox;
    Label footerLabel;
    TextField footerTextField;
    
    HBox layoutBox;
    Label templateLabel;
    Label colorLabel;
    Label fontLabel;
    ComboBox templateBox;
    ComboBox colorBox;
    ComboBox fontBox;
    EportfolioGeneratorFileController fileController;
   

    private EportfolioWorkspaceController workspaceController;
    
    EportfolioWorkspace(EportfolioPage initPage, EportfolioGeneratorFileController controller) {
        page = initPage;
        workspaceToolBar = new VBox();
        workspaceToolBar.getStyleClass().add(CSS_CLASS_WORKSPACE_CONTROL_TOOL_BAR);
        fileController = controller;
        
        addTextComponentButton = this.initChildButton(workspaceToolBar, ICON_ADD_TEXT_COMPONENT, TOOLTIP_ADD_TEXT_COMPONENT, CSS_CLASS_WORKSPACE_CONTROL_TOOL_BAR_BUTTON, false);
        addImageComponentButton = this.initChildButton(workspaceToolBar, ICON_ADD_IMAGE_COMPONENT, TOOLTIP_ADD_IMAGE_COMPONENT, CSS_CLASS_WORKSPACE_CONTROL_TOOL_BAR_BUTTON, false);
        addSlideshowComponentButton = this.initChildButton(workspaceToolBar, ICON_ADD_SLIDESHOW_COMPONENT, TOOLTIP_ADD_SLIDESHOW_COMPONENT, CSS_CLASS_WORKSPACE_CONTROL_TOOL_BAR_BUTTON, false);
        addVideoComponentButton = this.initChildButton(workspaceToolBar, ICON_ADD_VIDEO_COMPONENT, TOOLTIP_ADD_VIDEO_COMPONENT, CSS_CLASS_WORKSPACE_CONTROL_TOOL_BAR_BUTTON, false);
        editComponentButton = this.initChildButton(workspaceToolBar, ICON_EDIT_COMPONENT, TOOLTIP_EDIT_COMPONENT, CSS_CLASS_WORKSPACE_CONTROL_TOOL_BAR_BUTTON, true);
        editHyperlinkButton = this.initChildButton(workspaceToolBar, ICON_EDIT_HYPERLINK, TOOLTIP_EDIT_HYPERLINK, CSS_CLASS_WORKSPACE_CONTROL_TOOL_BAR_BUTTON, true);
        removeComponentButton = this.initChildButton(workspaceToolBar, ICON_REMOVE_COMPONENT, TOOLTIP_REMOVE_COMPONENT, CSS_CLASS_WORKSPACE_CONTROL_TOOL_BAR_BUTTON, true);
        
        pageDataPane = new GridPane();        
        personalDataPane = new GridPane();
        componentDataPane = new GridPane();
        componentDataPane.getStyleClass().add(CSS_CLASS_WORK_SPACE_COMPONENT_PADDING);
        
        
        personDataBox = new HBox();
        bannerImageLabel = new Label("Banner Image:   ");
        if(page.getBannerImgSrc().equals("null")){
        setBannerImageButton = new Button("Choice Banner Image");
        }
        else{
            setBannerImageButton = new Button(page.getBannerImgSrc());
        }
        studentNameLabel = new Label("Student Name:   ");
        studentNameTextField = new TextField(page.getStudentName());
        studentNameTextField.textProperty().addListener((observable, oldValue, newValue)->{
            page.setStudentName(newValue);
        });
        HBox.setMargin(setBannerImageButton, new Insets(0,80,10,0));

        personDataBox.getChildren().addAll(bannerImageLabel, setBannerImageButton, studentNameLabel, studentNameTextField);
        personDataBox.getStyleClass().add(CSS_CLASS_WORK_SPACE_PAGE_DATA_PANE);
        
        layoutBox = new HBox();
        GridPane.setHgrow(layoutBox, Priority.ALWAYS);
        templateLabel = new Label("Page template:  ");
        colorLabel = new Label("Page color: ");
        fontLabel = new Label("Page font: ");
        
        templateBox = new ComboBox();
        templateBox.getItems().addAll("Navigation_left","Navigation_mid","Navigation_next_banner","Navigation_right","Navigation_top");
        if(!page.getTemplate().equals("null"))
            templateBox.setValue(page.getTemplate());
        templateBox.setOnMousePressed(e -> {
            page.setTemplate((String) templateBox.getValue());
        });
        colorBox = new ComboBox();
        colorBox.getItems().addAll("Blue","Green","Orange","Pink","Yellow");
        if(!page.getColor().equals("null"))
            colorBox.setValue(page.getColor());
        colorBox.setOnMouseClicked(e->{
           page.setColor((String) colorBox.getValue());
       });
        fontBox = new ComboBox();
        fontBox.getItems().addAll("Comfortaa","Handlee","Indie Flower","Josefin Sans","Quicksand");
        if(!page.getFont().equals("null"))
            fontBox.setValue(page.getFont());
        fontBox.setOnMouseClicked(e->{

            page.setFont((String) fontBox.getValue());
        });
        HBox.setMargin(templateBox, new Insets(0,80,10,0));
        HBox.setMargin(colorBox, new Insets(0,80,10,0));
        HBox.setMargin(fontBox, new Insets(0,80,10,0));

        layoutBox.getChildren().addAll(templateLabel, templateBox, colorLabel, colorBox, fontLabel, fontBox);
        layoutBox.getStyleClass().add(CSS_CLASS_WORK_SPACE_PAGE_DATA_PANE);
        
        footerBox = new HBox();
        footerLabel = new Label("Footer Text:  ");
        
        footerTextField = new TextField();
        if(!page.getFooter().equals("This Website is Generated and Maintained by ePortfolio Generator")){
            footerTextField.setText(page.getFooter());
        }
        footerTextField.setMinWidth(100);
        footerTextField.textProperty().addListener((observable, oldValue, newValue)->{
            page.setFooter(newValue);
            footerTextField.setPrefWidth(footerTextField.getText().length()*7);
        });
        footerBox.getChildren().addAll(footerLabel, footerTextField);
        personalDataPane.add(personDataBox, 0, 0);
        footerBox.getStyleClass().add(CSS_CLASS_WORK_SPACE_PAGE_DATA_PANE);  
        
        personalDataPane.add(layoutBox, 0, 1);
                personalDataPane.add(footerBox, 0, 2);
        
        //GridPane.setHgrow(this, Priority.ALWAYS);

        
        pageDataPane.add(personalDataPane, 0, 0);
        pageDataPane.add(componentDataPane, 0, 1);
        workspacePane = new ScrollPane(pageDataPane);
        this.getChildren().add(workspaceToolBar);
        this.getChildren().add(workspacePane);
        
        HBox.setHgrow(workspacePane, Priority.ALWAYS);

        initWorkspaceEventHandler();
        reloadPageComponent();
        
    }

    EportfolioWorkspace(boolean selected) {
        if(!selected){
        workspaceToolBar = new VBox();
        workspaceToolBar.getStyleClass().add(CSS_CLASS_WORKSPACE_CONTROL_TOOL_BAR);
        
        addTextComponentButton = this.initChildButton(workspaceToolBar, ICON_ADD_TEXT_COMPONENT, TOOLTIP_ADD_TEXT_COMPONENT, CSS_CLASS_WORKSPACE_CONTROL_TOOL_BAR_BUTTON, true);
        addImageComponentButton = this.initChildButton(workspaceToolBar, ICON_ADD_IMAGE_COMPONENT, TOOLTIP_ADD_IMAGE_COMPONENT, CSS_CLASS_WORKSPACE_CONTROL_TOOL_BAR_BUTTON, true);
        addSlideshowComponentButton = this.initChildButton(workspaceToolBar, ICON_ADD_SLIDESHOW_COMPONENT, TOOLTIP_ADD_SLIDESHOW_COMPONENT, CSS_CLASS_WORKSPACE_CONTROL_TOOL_BAR_BUTTON, true);
        addVideoComponentButton = this.initChildButton(workspaceToolBar, ICON_ADD_VIDEO_COMPONENT, TOOLTIP_ADD_VIDEO_COMPONENT, CSS_CLASS_WORKSPACE_CONTROL_TOOL_BAR_BUTTON, true);
        editComponentButton = this.initChildButton(workspaceToolBar, ICON_EDIT_COMPONENT, TOOLTIP_EDIT_COMPONENT, CSS_CLASS_WORKSPACE_CONTROL_TOOL_BAR_BUTTON, true);
        editHyperlinkButton = this.initChildButton(workspaceToolBar, ICON_EDIT_HYPERLINK, TOOLTIP_EDIT_HYPERLINK, CSS_CLASS_WORKSPACE_CONTROL_TOOL_BAR_BUTTON, true);
        removeComponentButton = this.initChildButton(workspaceToolBar, ICON_REMOVE_COMPONENT, TOOLTIP_REMOVE_COMPONENT, CSS_CLASS_WORKSPACE_CONTROL_TOOL_BAR_BUTTON, true);
            
        this.getChildren().add(workspaceToolBar);
        this.getStyleClass().add(CSS_CLASS_WORKSPACE_EMPTY_PANE);
        }
    }
    public EportfolioPage getPageData(){
        return page;
    }
    
    private Button initChildButton(
            Pane toolbar, 
	    String iconFileName, 
	    LanguagePropertyType tooltip, 
	    String cssClass,
	    boolean disabled) {
	PropertiesManager props = PropertiesManager.getPropertiesManager();
	String imagePath = "file:" + PATH_ICON + iconFileName;
	Image buttonImage = new Image(imagePath);
	javafx.scene.control.Button button = new javafx.scene.control.Button();
	button.getStyleClass().add(cssClass);
	button.setDisable(disabled);
	button.setGraphic(new ImageView(buttonImage));
	Tooltip buttonTooltip = new Tooltip(props.getProperty(tooltip.toString()));
	button.setTooltip(buttonTooltip);
	toolbar.getChildren().add(button);
	return button;
    }

    private void initWorkspaceEventHandler() {
        workspaceController = new EportfolioWorkspaceController(this);
        setBannerImageButton.setOnAction(e->{
            workspaceController.handleSetBannerRequest();
            setBannerImageButton.setText(page.getBannerImgSrc());
            fileController.markAsEdited();
        });
        templateBox.setOnAction(e->{
            page.setTemplate((String) templateBox.getValue());
                        fileController.markAsEdited();

        });
        colorBox.setOnAction(e->{
            page.setColor((String) colorBox.getValue());
                        fileController.markAsEdited();

        });
        fontBox.setOnAction(e->{
            page.setFont((String) fontBox.getValue());
                        fileController.markAsEdited();

        });
        addTextComponentButton.setOnAction(e->{
            workspaceController.handleAddTextRequest();
                        fileController.markAsEdited();

        });
        addImageComponentButton.setOnAction(e->{
            workspaceController.handleAddImageRequest();
                        fileController.markAsEdited();

        });
        addSlideshowComponentButton.setOnAction(e->{
            workspaceController.handleAddSlideshowRequest();
                        fileController.markAsEdited();

        });
        addVideoComponentButton.setOnAction(e->{
            workspaceController.handleAddVideoRequest();
                        fileController.markAsEdited();

        });
        editComponentButton.setOnAction(e->{
            workspaceController.handleEditRequest();
                        fileController.markAsEdited();

        });
        editHyperlinkButton.setOnAction(e->{
            workspaceController.handleEditHyperlinkRequest();
                        fileController.markAsEdited();

        });
        removeComponentButton.setOnAction(e->{
            workspaceController.handleRemoveRequest();
                        fileController.markAsEdited();

        });
        
    }
    
    public void reloadPageComponent(){
        componentDataPane.getChildren().clear();
        for(EportfolioComponent component : page.getComponents()){
            EportfolioComponentView componentView = new EportfolioComponentView(component);
            GridPane.setMargin(componentView, new Insets(20,10,0,0));
            componentDataPane.add(componentView, 0, i);
            i++;
             HBox.setHgrow(componentView, Priority.ALWAYS);
            componentView.getStyleClass().add(CSS_CLASS_WORK_SPACE_PAGE_DATA_PANE);
            componentView.setOnMouseClicked(e->{
                workspaceController.handleSelectedRequest(component);
            });
            if(component == page.getSelectedComponent()){
                componentView.getStyleClass().add(CSS_CLASS_COMPONENT_SLELECTED);
            }
        }
    }

    public void updateControls() {
        if(page.isSelected()){
            removeComponentButton.setDisable(false);
            editComponentButton.setDisable(false);
            if(page.getSelectedComponent().getType().equals("text")){
                editHyperlinkButton.setDisable(false);

            }
            else{
                editHyperlinkButton.setDisable(true);
            }
        }
    }

    
}
