/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epg.view;

import static epg.StartupConstants.CSS_CLASS_WORKSPACE_COMPONENT_VIEW;
import static epg.StartupConstants.CSS_TEXT_SELECTION;
import epg.model.EportfolioComponent;
import epg.model.EportfolioImageComponent;
import epg.model.EportfolioSlide;
import epg.model.EportfolioSlideshowComponent;
import epg.model.EportfolioTextComponent;
import epg.model.EportfolioVideoComponent;
import epg.model.EportfolioList;
import javafx.geometry.HPos;
import javafx.geometry.VPos;
import javafx.scene.control.Label;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Circle;

/**
 *
 * @author wqu
 */
public class EportfolioComponentView extends HBox {
    VBox dataContainer;
    GridPane dataPane;
    Label type;
    Label srcData;

    EportfolioComponentView(EportfolioComponent component) {
        dataContainer = new VBox();
        dataPane = new GridPane();
        
        
        if(component.getType().equals("text")){
            type = new Label("Text component");
            EportfolioTextComponent componentToView = (EportfolioTextComponent)component;
            dataPane.add(new Label("Text type:  "), 0, 0);
            dataPane.add(new Label(componentToView.getSrcType()), 1, 0);
            dataPane.add(new Label("Text Sources:   "), 0, 1);
            
            if(componentToView.getSrcType().equals("Header") || componentToView.getSrcType().equals("Paragraph")){
            int x = 1;
            VBox srcContainer = new VBox();
            HBox srcBox = new HBox();
            Label srcLabel;
            Label hyperlinkLabel;
            for(int y = 0; y < componentToView.getSrcs().size(); y++){
                if(componentToView.getSrcs().get(y)!=null){
                    srcLabel= new Label(componentToView.getSrcs().get(y));
                    srcLabel.setWrapText(true);
                     srcBox.getChildren().add(srcLabel);
                }
                if((y< componentToView.getHyperlinks().size()) && (componentToView.getHyperlinks().get(y) !=null)){
                    hyperlinkLabel = new Label(componentToView.getHyperlinks().get(y));
                    hyperlinkLabel.setWrapText(true);
                    hyperlinkLabel.getStyleClass().add(CSS_TEXT_SELECTION);
                    srcBox.getChildren().add(hyperlinkLabel);
                }


            }
            srcContainer.getChildren().add(srcBox);
            dataPane.add(srcContainer, 1, x);
                x++;
            
            
            if(componentToView.getSrcType().equals("Paragraph")){
                dataPane.add(new Label("Paragraph Font:  "), 0, x);
                Label fontLabel = new Label(componentToView.getFont());
                dataPane.add(fontLabel, 1, x);
            }
            }
            else if(componentToView.getSrcType().equals("List")){
                int x = 1;
                VBox srcContainer;
                HBox srcBox;
                Label srcLabel;
                Label hyperlinkLabel;
                for(EportfolioList list : componentToView.getListSrcs()){
                    srcContainer = new VBox();
                    srcBox = new HBox();
                    for(int y = 0; y <list.getSrcs().size(); y++){
                        if(list.getSrcs().get(y) !=null){
                            srcLabel = new Label(list.getSrcs().get(y));
                            srcBox.getChildren().add(srcLabel);
                        }
                        if((y< list.getHyperlinkSrcs().size()) &&(list.getHyperlinkSrcs().get(y)!=null)){
                            hyperlinkLabel = new Label(list.getHyperlinkSrcs().get(y));
                            hyperlinkLabel.getStyleClass().add(CSS_TEXT_SELECTION);
                            srcBox.getChildren().add(hyperlinkLabel);
                        }
                    }
                                            srcContainer.getChildren().add(srcBox);

                    dataPane.add(srcContainer, 1, x);
                    x++;
                }
            }
            
        }
        else if(component.getType().equals("image")){
            type = new Label("Image component");
            EportfolioImageComponent componentToView = (EportfolioImageComponent)component;
            dataPane.add(new Label("Image source:   "), 0, 0);
            dataPane.add(new Label(componentToView.getImageSrc()), 1, 0);
            dataPane.add(new Label("Image caption:  "), 0, 1);
            dataPane.add(new Label(componentToView.getCaption()), 1, 1);
            dataPane.add(new Label("Image Width:    "), 0, 2);
            dataPane.add(new Label(Integer.toString(componentToView.getWidth()) + "%"), 1, 2);
            dataPane.add(new Label("Image Height:   "), 0, 3);
            dataPane.add(new Label(Integer.toString(componentToView.getHiehgt()) + "%"), 1, 3);
            dataPane.add(new Label("Image Display Option:   "), 0, 4);
            dataPane.add(new Label(componentToView.getImageOption()), 1, 4);
            
            
        }
        else if(component.getType().equals("video")){
            type = new Label("Video component");
            EportfolioVideoComponent componentToView = (EportfolioVideoComponent)component;
            dataPane.add(new Label("Video source:   "), 0, 0);
            dataPane.add(new Label(componentToView.getVideoSrc()), 1, 0);
            dataPane.add(new Label("Video caption:  "), 0, 1);
            dataPane.add(new Label(componentToView.getCaption()), 1, 1);
            dataPane.add(new Label("Video Width:    "), 0, 2);
            dataPane.add(new Label(Integer.toString(componentToView.getWidth()) + "%"), 1, 2);
            dataPane.add(new Label("Video Height:   "), 0, 3);
            dataPane.add(new Label(Integer.toString(componentToView.getWidth()) + "%"), 1, 3);
        }
        else if(component.getType().equals("slideshow")){
            type = new Label("Slideshow component");
            EportfolioSlideshowComponent componentToView = (EportfolioSlideshowComponent)component;
            dataPane.add(new Label("Slideshow sources & captions:   "), 0, 0);
            int x = 0;
            for(EportfolioSlide slide : componentToView.getSrcs()){
                Label src = new Label("Image source: "+ slide.getSrc());
                dataPane.add(src, 1, x);
                x++;
                Label caption = new Label("Image caption: "+ slide.getCaption());
                dataPane.add(caption, 1, x);
                x++;
            }
            //dataPane.add(new Label(componentToView.getVideoSrc()), 1, 0);
        }
        
        dataContainer.getChildren().addAll(type, dataPane);
        this.getChildren().add(dataContainer);
        this.setPrefWidth(1319);
        this.setPrefHeight(242);
        this.getStyleClass().add(CSS_CLASS_WORKSPACE_COMPONENT_VIEW);
    }

    
}
