/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epg.view;

import static epg.StartupConstants.CSS_CLASS_PAGE_EDIT_HBOX;
import epg.controller.EportfolioGeneratorFileController;
import epg.model.EportfolioPage;
import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;

/**
 *
 * @author Wenjun
 */
public class EportfolioPageEditView extends HBox{
    EportfolioPage page;
    EportfolioGeneratorFileController fileController;
    
    Label titleLabel;
    TextField titleTextField;
    boolean selected = false;
    EportfolioPageEditView(EportfolioPage initPage, EportfolioGeneratorFileController initFileController ,int PageIndex) {
        fileController = initFileController;
        page = initPage;
        
        titleLabel = new Label("Page Title:");
        titleTextField = new TextField();
        getChildren().add(titleLabel);
        getChildren().add(titleTextField);
        this.setPadding(new Insets(40,40,40,40));
        this.getStyleClass().add(CSS_CLASS_PAGE_EDIT_HBOX);
        
        titleTextField.textProperty().addListener((observable, oldValue, newValue)->{
            page.setTitle(newValue);
            page.getNavigation().set(PageIndex, newValue);
           // fileController.markAsEdited();
        });
        
    }

    void setTitleText(String title) {
        titleTextField.setText(title);
    }
    
}
