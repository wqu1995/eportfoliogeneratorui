package epg;

/**
 * This class provides setup constants for initializing the application
 * that are NOT language dependent.
 * 
 * @author McKilla Gorilla & Wenjun Qu
 */
public class StartupConstants {

    // WE'LL LOAD ALL THE UI AND LANGUAGE PROPERTIES FROM FILES,
    // BUT WE'LL NEED THESE VALUES TO START THE PROCESS

    public static String PROPERTY_TYPES_LIST = "property_types.txt";
    public static String UI_PROPERTIES_FILE_NAME_EN = "properties_EN.xml";
    public static String PROPERTIES_SCHEMA_FILE_NAME = "properties_schema.xsd";
    public static String PATH_DATA = "./data/";
    public static String PATH_WEB_VIEW = PATH_DATA+"sample_view/";
    public static String PATH_SAVE = PATH_DATA +"save/";
    public static String PATH_ICON = "./icons/";
    public static String PATH_CSS = "epg/style/";
        public static String STYLE_SHEET_UI = PATH_CSS + "EportfolioGeneratorStyle.css";


    
    
    // HERE ARE THE LANGUAGE INDEPENDENT GUI ICONS
    public static String WINDOW_ICON = "window_title.jpg";
    public static String ICON_NEW_WORK = "New.png";
    public static String ICON_LOAD_WORK = "Load.png";
    public static String ICON_SAVE_WORK = "Save.png";
    public static String ICON_SAVE_AS_WORK = "Save_as.png";
    public static String ICON_EXPORT_WORK = "Export.png";
    public static String ICON_EXIT = "Exit.png";
    public static String ICON_ADD_PAGE = "Add.png";
    public static String ICON_REMOVE_PAGE = "Remove.png";
    public static String ICON_PAGE_UP = "Page_up.png";
    public static String ICON_PAGE_DOWN = "Page_down.png";
    public static String ICON_PREVIOUS = "Previous.png";
    public static String ICON_NEXT = "Next.png";
    public static String ICON_ADD_TEXT_COMPONENT = "Add_text.png";
    public static String ICON_ADD_IMAGE_COMPONENT = "Add_image.png";
    public static String ICON_ADD_SLIDESHOW_COMPONENT = "Add_slideshow.png";
    public static String ICON_ADD_VIDEO_COMPONENT = "Add_video.png";
    public static String ICON_EDIT_COMPONENT = "Edit_component.png";
    public static String ICON_EDIT_HYPERLINK = "Edit_hyperlink.png";
    public static String ICON_REMOVE_COMPONENT = "Remove_component.png";


    // @todo
    
    // CSS STYLE SHEET CLASSES
    public static String    CSS_CLASS_VERTICAL_TOOLBAR_BUTTON = "vertical_toolbar_button";
    public static String    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON = "horizontal_toolbar_button";
    public static String    CSS_CLASS_SLIDE_SHOW_EDIT_VBOX = "slide_show_edit_vbox";
    public static String    CSS_CLASS_SLIDE_EDIT_VIEW = "slide_edit_view";
    public static String    CSS_CLASS_SLIDE_BORDER = "slide_slected";
    public static String    CSS_CLASS_SLIDE_BUTTON_SPACING = "slide_view_hbox";
    public static String    CSS_LANGUAGE_SELECTION_ALERT = "language_selection";
    public static String    CSS_LANGUAGE_SELECTION_ALERT_BUTTON = "language_selection_button";
    public static String    CSS_CLASS_SLIDE_SHOW_WORK_SPACE = "slide_show_workspace";
    public static String    CSS_CLASS_SLIDE_SHOW_EDITOR_PANE = "slide_editor_pane";
    public static String    CSS_CLASS_FONT  = "slide_show_font";
    public static String    CSS_CLASS_CAPTION_FONT = "slide_show_caption_font";
    public static String    CSS_CLASS_SLIDE_SHOW_FILE_TOOL_BAR = "slide_show_tool_bar";
    public static String    CSS_CLASS_MAINUI_FILE_TOOL_BAR = "eportfolio_file_tool_bar";
        public static String    CSS_CLASS_PAGE_CONTROL_TOOL_BAR = "eportfolio_page_control_tool_bar";
    public static String    CSS_CLASS_MAINUI_FILE_TOOL_BAR_BUTTON = "eportfolio_file_tool_bar_button";
    public static String    CSS_CLASS_PAGE_CONTROL_TOOL_BAR_BUTTON = "eportfoli_page_control_tool_bar_button";
    public static String    CSS_CLASS_WORKSPACE_BUTTON = "slide_show_tool_bar_button";
    public static String    CSS_CLASS_WORK_TITLE_VBOX = "eportfolio_work_title_vbox";
    public static String    CSS_CLASS_PAGE_EDIT_HBOX = "eportfolio_page_edit_hbox";
        public static String    CSS_CLASS_PAGE_SCROLL_PANE = "eportfolio_page_scroll_pane";
    public static String    CSS_CLASS_WORK_SPACE_PANE = "eportfolio_work_space_pane";
    public static String    CSS_CLASS_WORKSPACE_CONTROL_TOOL_BAR_BUTTON = "eportfoli_workspace_control_tool_bar_button";
    public static String    CSS_CLASS_WORKSPACE_CONTROL_TOOL_BAR = "eportfolio_workspace_control_tool_bar";
    public static String    CSS_CLASS_WORKSPACE_COMPONENT_VIEW = "eportfolio_component_view";
    public static String    CSS_CLASS_COMPONENT_SLELECTED = "eportfolio_component_selected";
    public static String    CSS_CLASS_WORK_SPACE_PAGE_DATA_PANE = "eportfolio_workspace_page_data_pane";
    public static String    CSS_CLASS_WORK_SPACE_COMPONENT_PADDING = "eportfolio_workspace_component_padding";
    public static String    CSS_CLASS_WORKSPACE_EMPTY_PANE = "eportfolio_workspace_empty_pane";
    public static String    CSS_CLASS_SELECTED = "eportfolio_selected";
    public static String    CSS_TEXT_SELECTION = "paragraph_text_selection";

    
    // UI LABELS


}
