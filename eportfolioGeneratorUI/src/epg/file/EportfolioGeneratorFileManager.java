/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epg.file;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import static epg.StartupConstants.PATH_SAVE;
import epg.model.EportfolioComponent;
import epg.model.EportfolioImageComponent;
import epg.model.EportfolioList;
import epg.model.EportfolioPage;
import epg.model.EportfolioPageModel;
import epg.model.EportfolioSlide;
import epg.model.EportfolioSlideshowComponent;
import epg.model.EportfolioTextComponent;
import epg.model.EportfolioVideoComponent;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import javafx.collections.ObservableList;
import properties_manager.PropertiesManager;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonWriter;
import javax.json.stream.JsonParser;


/**
 *
 * @author wqu
 */
public class EportfolioGeneratorFileManager {
    public static String JSON_EXT = ".json";
    public static String JSON_WORK_TITLE = "ePortfolio_work_title";
    public static String JSON_PAGE_INDEX = "pages";
    public static String JSON_PAGE_TITLE = "page_title";
    public static String JSON_PAGE_STUDENT_NAME = "page_student_name";
    public static String JSON_PAGE_BANNER_IMG_SRC = "page_banner";
    public static String JSON_PAGE_LAYOUT_TEMPLATE = "page_layout";
    public static String JSON_PAGE_COLOR_TEMPALTE = "page_color";
    public static String JSON_PAGE_FONT_TEMPALTE = "page_font";
    public static String JSON_PAGE_FOOTER_NOTES = "page_footer";
    public static String JSON_PAGE_COMPONENTS = "page_components";
    public static String JSON_COMPONENT_TYPE = "component_type";
    public static String JSON_COMPONENT_SRCS = "component_srcs";
    public static String JSON_TEXT_TYPE = "text_type";
    public static String JSON_TEXT_SRCS = "text_srcs";
    public static String JSON_TEXT_HYPERLINK_SRCS = "text_hyperlink_srcs";
    public static String JSON_TEXT_HYPERLINK_DISTS = "text_hyperlink_dists";
    public static String JSON_TEXT_SRC = "text_src";
    public static String JSON_TEXT_HYPERLINK_SRC = "text_hyperlink_src";
    public static String JSON_TEXT_HYPERLINK_DIST = "text_hyperlink_dist";
    public static String JSON_TEXT_FONT = "text_font";
    public static String JSON_LIST_SRCS = "list_srcs";
    public static String JSON_LIST_SRC = "list_src";
    public static String JSON_LIST_HYPERLINK_SRCS = "list_hyperlink_srcs";
    public static String JSON_LIST_HYPERLINK_SRC = "list_hyperlink_src";
    public static String JSON_LIST_HYPERLINK_DISTS = "list_hyperlink_dists";
    public static String JSON_LIST_HYPERLINK_DIST = "list_hyperlink_dist";
    public static String JSON_IMG_SRC = "img_src";
    public static String JSON_IMG_CAPTION = "img_caption";
    public static String JSON_IMG_WIDTH = "img_width";
    public static String JSON_IMG_HEIGHT = "img_height";
    public static String JSON_IMG_OPTION = "img_option";
    public static String JSON_VIDEO_SRC = "video_src";
    public static String JSON_VIDEO_CAPTION = "video_caption";
    public static String JSON_VIDEO_WIDTH = "video_width";
    public static String JSON_VIDEO_HEIHGT = "video_height";
    public static String JSON_SLIDE_SRC = "slideshow_src";
    public static String JSON_SLIDE_CAPTION = "slideshow_caption";




    public void saveWork(EportfolioPageModel pageModel, String path) throws IOException {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        
        OutputStream os = new FileOutputStream (path);
        JsonWriter jsonWriter = Json.createWriter(os);
        
        JsonArray pageJsonArray = makeEportfolioJsonArray(pageModel.getPages());
        
        JsonObject pageJsonObject = Json.createObjectBuilder()
                                        .add(JSON_PAGE_INDEX, pageJsonArray)
                                        .build();
        
        //Gson prettyGson = new GsonBuilder().setPrettyPrinting().create();
        //String test = prettyGson.toJson(pageJsonObject);
        jsonWriter.writeObject(pageJsonObject);
    }
    
    public void loadWork(EportfolioPageModel pageToLoad, String jsonFilePath) throws IOException{
        JsonObject json = loadJSONFile(jsonFilePath);
        pageToLoad.reset();
        JsonArray pageJsonArray = json.getJsonArray(JSON_PAGE_INDEX);
        for(int i = 0; i < pageJsonArray.size(); i++){
            JsonObject pageJso = pageJsonArray.getJsonObject(i);
            pageToLoad.addPage(pageJso.getString(JSON_PAGE_TITLE));
            loadPageData(pageToLoad.getPages().get(i), pageJso);
        }
        
    }
    
    private JsonArray makeEportfolioJsonArray(ObservableList<EportfolioPage> pages) {
        JsonArrayBuilder jsb = Json.createArrayBuilder();
        for(EportfolioPage page : pages){
            JsonObject jso = makePageObject(page);
            jsb.add(jso);
        }
        JsonArray jA = jsb.build();
        return jA;
    }

    private JsonObject makePageObject(EportfolioPage page) {
        JsonArrayBuilder jsb = Json.createArrayBuilder();
        EportfolioMakeJsonObject objMaker = new EportfolioMakeJsonObject();
        for(EportfolioComponent component : page.getComponents()){
            JsonObject jso = objMaker.jsonObjectMaker(component);
            jsb.add(jso);
        }
        JsonArray componentArray = jsb.build();
        JsonObject jso = Json.createObjectBuilder().add(JSON_PAGE_TITLE, page.getTitle())
                                                   .add(JSON_PAGE_STUDENT_NAME, page.getName())
                                                   .add(JSON_PAGE_BANNER_IMG_SRC, page.getBannerImgSrc())
                                                   .add(JSON_PAGE_LAYOUT_TEMPLATE, page.getTemplate())
                                                   .add(JSON_PAGE_COLOR_TEMPALTE, page.getColor())
                                                   .add(JSON_PAGE_FONT_TEMPALTE, page.getFont())
                                                   .add(JSON_PAGE_FOOTER_NOTES, page.getFooter() )
                                                   .add(JSON_PAGE_COMPONENTS,componentArray )
                                                   .build();
            return jso;
    }



    private JsonObject loadJSONFile(String jsonFilePath) throws IOException {
        InputStream is = new FileInputStream(jsonFilePath);
        JsonReader jsonReader = Json.createReader(is);
        JsonObject json = jsonReader.readObject();
        jsonReader.close();
        is.close();
        return json;        
    }

    private void loadPageData(EportfolioPage pageToLoad, JsonObject pageData) {
        pageToLoad.setStudentName(pageData.getString(JSON_PAGE_STUDENT_NAME));
        pageToLoad.setBannerImgSrc(pageData.getString(JSON_PAGE_BANNER_IMG_SRC));
        pageToLoad.setTemplate(pageData.getString(JSON_PAGE_LAYOUT_TEMPLATE));
        pageToLoad.setColor(pageData.getString(JSON_PAGE_COLOR_TEMPALTE));
        pageToLoad.setFont(pageData.getString(JSON_PAGE_FONT_TEMPALTE));
        pageToLoad.setFooter(pageData.getString(JSON_PAGE_FOOTER_NOTES));
        JsonArray componentArray = pageData.getJsonArray(JSON_PAGE_COMPONENTS);
        for(int i = 0; i< componentArray.size(); i++){
            JsonObject jso = componentArray.getJsonObject(i);
            
            if(jso.getString(JSON_COMPONENT_TYPE).equals("text")){
                EportfolioTextComponent componentToAdd = new EportfolioTextComponent(jso.getString(JSON_COMPONENT_TYPE));
                componentToAdd.setType(jso.getString(JSON_COMPONENT_TYPE));
                JsonArray srcArray = jso.getJsonArray(JSON_COMPONENT_SRCS);
                JsonObject srcJson = srcArray.getJsonObject(0);
                componentToAdd.setSrcType(srcJson.getString(JSON_TEXT_TYPE));
                if(srcJson.getString(JSON_TEXT_TYPE).equals("Header") ||(srcJson.getString(JSON_TEXT_TYPE).equals("Paragraph"))){
                    JsonArray textSrcArray = srcJson.getJsonArray(JSON_TEXT_SRCS);
                    for(int x = 0; x < textSrcArray.size();x++){
                        JsonObject jst = textSrcArray.getJsonObject(x);
                        componentToAdd.getSrcs().add(jst.getString(JSON_TEXT_SRC));
                    }
                    JsonArray textHyperlinkSrcArray = srcJson.getJsonArray(JSON_TEXT_HYPERLINK_SRCS);
                    for(int x = 0; x < textHyperlinkSrcArray.size();x++){
                        JsonObject jst = textHyperlinkSrcArray.getJsonObject(x);
                        componentToAdd.getHyperlinks().add(jst.getString(JSON_TEXT_HYPERLINK_SRC));
                    }
                    JsonArray textHyperlinkDistArray = srcJson.getJsonArray(JSON_TEXT_HYPERLINK_DISTS);
                    for(int x = 0; x < textHyperlinkDistArray.size();x++){
                        JsonObject jst = textHyperlinkDistArray.getJsonObject(x);
                        componentToAdd.getHyperlinkDists().add(jst.getString(JSON_TEXT_HYPERLINK_DIST));
                    }
                    if(srcJson.getString(JSON_TEXT_TYPE).equals("Paragraph")){
                        componentToAdd.setFont(srcJson.getString(JSON_TEXT_FONT));
                    }
                }
                else if(srcJson.getString(JSON_TEXT_TYPE).equals("List")){
                    JsonArray listArray = srcJson.getJsonArray(JSON_TEXT_SRCS);
                    for(int x= 0; x< listArray.size(); x++){
                        JsonObject listJso = listArray.getJsonObject(x);
                        EportfolioList list = new EportfolioList();
                        JsonArray listSrcArray = listJso.getJsonArray(JSON_LIST_SRCS);
                        for(int y = 0; y<listSrcArray.size(); y++){
                            JsonObject jst = listSrcArray.getJsonObject(y);
                            list.getSrcs().add(jst.getString(JSON_LIST_SRC));
                        }
                        JsonArray listHyperlinkSrcArray = listJso.getJsonArray(JSON_LIST_HYPERLINK_SRCS);
                        for(int y = 0; y<listHyperlinkSrcArray.size(); y++){
                            JsonObject jst = listHyperlinkSrcArray.getJsonObject(y);
                            list.getHyperlinkSrcs().add(jst.getString(JSON_LIST_HYPERLINK_SRC));
                        }
                        JsonArray listHyperlinkDistArray = listJso.getJsonArray(JSON_LIST_HYPERLINK_DISTS);
                        for(int y = 0; y<listHyperlinkDistArray.size(); y++){
                            JsonObject jst = listHyperlinkDistArray.getJsonObject(y);
                            list.getHyperlinkDists().add(jst.getString(JSON_LIST_HYPERLINK_DIST));
                        }
                        componentToAdd.getListSrcs().add(list);
                    }
                }
                
                pageToLoad.getComponents().add(componentToAdd);

            }
            else if( jso.getString(JSON_COMPONENT_TYPE ).equals("image")){
                
                EportfolioImageComponent componentToAdd = new EportfolioImageComponent(jso.getString(JSON_COMPONENT_TYPE));
                componentToAdd.setType(jso.getString(JSON_COMPONENT_TYPE));
                JsonArray srcArray = jso.getJsonArray(JSON_COMPONENT_SRCS);
                JsonObject srcJson = srcArray.getJsonObject(0);
                componentToAdd.setImageSrc(srcJson.getString(JSON_IMG_SRC));
                componentToAdd.setCaption(srcJson.getString(JSON_IMG_CAPTION));
                componentToAdd.setWidth(srcJson.getInt(JSON_IMG_WIDTH));
                componentToAdd.setHeight(srcJson.getInt(JSON_IMG_HEIGHT));
                componentToAdd.setDisplayOption(srcJson.getString(JSON_IMG_OPTION));
                
                pageToLoad.getComponents().add(componentToAdd);

            }
            else if(jso.getString(JSON_COMPONENT_TYPE).equals("video")){
                EportfolioVideoComponent componentToAdd = new EportfolioVideoComponent(jso.getString(JSON_COMPONENT_TYPE));
                componentToAdd.setType(jso.getString(JSON_COMPONENT_TYPE));
                JsonArray srcArray = jso.getJsonArray(JSON_COMPONENT_SRCS);
                JsonObject srcJson = srcArray.getJsonObject(0);
                componentToAdd.setVideoSrc(srcJson.getString(JSON_VIDEO_SRC));
                componentToAdd.setVideoCaption(srcJson.getString(JSON_VIDEO_CAPTION));
                componentToAdd.setVideoWidth(srcJson.getInt(JSON_VIDEO_WIDTH));
                componentToAdd.setVideoHeight(srcJson.getInt(JSON_VIDEO_HEIHGT));
                
                pageToLoad.getComponents().add(componentToAdd);

            }
            else if(jso.getString(JSON_COMPONENT_TYPE ).equals("slideshow")){
                EportfolioSlideshowComponent componentToAdd = new EportfolioSlideshowComponent(jso.getString(JSON_COMPONENT_TYPE));
                componentToAdd.setType(jso.getString(JSON_COMPONENT_TYPE));
                JsonArray srcArray = jso.getJsonArray(JSON_COMPONENT_SRCS);
                for(int s = 0; s< srcArray.size(); s++){
                    JsonObject srcJson = srcArray.getJsonObject(s);
                    EportfolioSlide temp = new EportfolioSlide(srcJson.getString(JSON_SLIDE_SRC), srcJson.getString(JSON_SLIDE_CAPTION));
                    componentToAdd.getSrcs().add(temp);
                }
                pageToLoad.getComponents().add(componentToAdd);
            }
        }
    }
    
}
