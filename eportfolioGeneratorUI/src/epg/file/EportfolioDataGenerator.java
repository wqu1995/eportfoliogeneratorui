/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epg.file;

import static epg.StartupConstants.PATH_WEB_VIEW;
import epg.model.EportfolioComponent;
import epg.model.EportfolioImageComponent;
import epg.model.EportfolioPage;
import epg.model.EportfolioSlide;
import epg.model.EportfolioSlideshowComponent;
import epg.model.EportfolioVideoComponent;
import static epg.view.EportfolioWebViewSpace.SLASH;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

/**
 *
 * @author wqu
 */
public class EportfolioDataGenerator {
    File pageDir;
    File dataDir;
    File imgDir;
    File videoDir;
    public EportfolioDataGenerator(EportfolioPage page) throws IOException{
                pageDir = new File(PATH_WEB_VIEW);
        dataDir = new File(PATH_WEB_VIEW +"data/");
        imgDir = new File(PATH_WEB_VIEW+"img/");
        videoDir = new File(PATH_WEB_VIEW+"video/");
        if(dataDir.exists()){
            File[] entries = dataDir.listFiles();
            for(File f: entries){
                f.delete();
            }
        }
        if(imgDir.exists()){
            File[] entries = imgDir.listFiles();
            for(File f: entries){
                f.delete();
            }
        }
        if(videoDir.exists()){
            File[] entries = videoDir.listFiles();
            for(File f: entries){
                f.delete();
            }
        }
        dataDir.mkdir();
        imgDir.mkdir();
        videoDir.mkdir();
        
        if(!page.getBannerImgSrc().equals("null")){
                    File bannerTemp = new File(page.getBannerImgSrc());
        File bannerImg = new File(imgDir.getPath()+ SLASH + page.getBannerImgSrc().substring(page.getBannerImgSrc().lastIndexOf("\\")+1));
        if(!bannerImg.exists()){
            Files.copy(bannerTemp.toPath(), bannerImg.toPath());
        }
        }
        for(EportfolioComponent component : page.getComponents()){
            if (component.getType().equals("image")){
                File imgTemp = new File((((EportfolioImageComponent)component).getImageSrc()));
                File img = new File(imgDir.getPath()+SLASH+(((EportfolioImageComponent)component).getImageSrc()).substring((((EportfolioImageComponent)component).getImageSrc()).lastIndexOf("\\")+1));
                if(!img.exists()){
                    Files.copy(imgTemp.toPath(), img.toPath());
                }
            }
            else if(component.getType().equals("slideshow")){
                for(EportfolioSlide slide : ((EportfolioSlideshowComponent)component).getSrcs()){
                    File imgTemp = new File(slide.getSrc());
                    File img = new File(imgDir.getPath()+SLASH + slide.getSrc().substring(slide.getSrc().lastIndexOf("\\")+1));
                    if(!img.exists()){
                        Files.copy(imgTemp.toPath(), img.toPath());
                    }
                }
            }
            else if(component.getType().equals("video")){
                File videoTemp =new File(((EportfolioVideoComponent)component).getVideoSrc());
                File video = new File(videoDir.getPath()+SLASH+ ((EportfolioVideoComponent)component).getVideoSrc().substring(((EportfolioVideoComponent)component).getVideoSrc().lastIndexOf("\\")+1));
                if(!video.exists()){
                    Files.copy(videoTemp.toPath(), video.toPath());
                }
            }
        }

    }
}
