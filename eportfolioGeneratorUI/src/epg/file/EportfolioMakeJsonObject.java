/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epg.file;

import static epg.file.EportfolioGeneratorFileManager.JSON_COMPONENT_SRCS;
import static epg.file.EportfolioGeneratorFileManager.JSON_COMPONENT_TYPE;
import static epg.file.EportfolioGeneratorFileManager.JSON_IMG_CAPTION;
import static epg.file.EportfolioGeneratorFileManager.JSON_IMG_HEIGHT;
import static epg.file.EportfolioGeneratorFileManager.JSON_IMG_OPTION;
import static epg.file.EportfolioGeneratorFileManager.JSON_IMG_SRC;
import static epg.file.EportfolioGeneratorFileManager.JSON_IMG_WIDTH;
import static epg.file.EportfolioGeneratorFileManager.JSON_LIST_HYPERLINK_DIST;
import static epg.file.EportfolioGeneratorFileManager.JSON_LIST_HYPERLINK_DISTS;
import static epg.file.EportfolioGeneratorFileManager.JSON_LIST_HYPERLINK_SRC;
import static epg.file.EportfolioGeneratorFileManager.JSON_LIST_HYPERLINK_SRCS;
import static epg.file.EportfolioGeneratorFileManager.JSON_LIST_SRC;
import static epg.file.EportfolioGeneratorFileManager.JSON_LIST_SRCS;
import static epg.file.EportfolioGeneratorFileManager.JSON_SLIDE_CAPTION;
import static epg.file.EportfolioGeneratorFileManager.JSON_SLIDE_SRC;
import static epg.file.EportfolioGeneratorFileManager.JSON_TEXT_FONT;
import static epg.file.EportfolioGeneratorFileManager.JSON_TEXT_HYPERLINK_DIST;
import static epg.file.EportfolioGeneratorFileManager.JSON_TEXT_HYPERLINK_DISTS;
import static epg.file.EportfolioGeneratorFileManager.JSON_TEXT_HYPERLINK_SRC;
import static epg.file.EportfolioGeneratorFileManager.JSON_TEXT_HYPERLINK_SRCS;
import static epg.file.EportfolioGeneratorFileManager.JSON_TEXT_SRC;
import static epg.file.EportfolioGeneratorFileManager.JSON_TEXT_SRCS;
import static epg.file.EportfolioGeneratorFileManager.JSON_TEXT_TYPE;
import static epg.file.EportfolioGeneratorFileManager.JSON_VIDEO_CAPTION;
import static epg.file.EportfolioGeneratorFileManager.JSON_VIDEO_HEIHGT;
import static epg.file.EportfolioGeneratorFileManager.JSON_VIDEO_SRC;
import static epg.file.EportfolioGeneratorFileManager.JSON_VIDEO_WIDTH;
import epg.model.EportfolioComponent;
import epg.model.EportfolioImageComponent;
import epg.model.EportfolioList;
import epg.model.EportfolioSlide;
import epg.model.EportfolioSlideshowComponent;
import epg.model.EportfolioTextComponent;
import epg.model.EportfolioVideoComponent;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;

/**
 *
 * @author wqu
 */
public class EportfolioMakeJsonObject {
    public EportfolioMakeJsonObject(){}
    public JsonObject jsonObjectMaker(EportfolioComponent component){
        JsonArrayBuilder jsb = Json.createArrayBuilder();
        switch (component.getType()){
            case "text":{
                EportfolioTextComponent textComponent = (EportfolioTextComponent)component;
                if(textComponent.getSrcType().equals("Header") || (textComponent.getSrcType().equals("Paragraph"))){
                    JsonArrayBuilder srcBuilder = Json.createArrayBuilder();
                    for(String src : textComponent.getSrcs()){
                        JsonObject jso = Json.createObjectBuilder().add(JSON_TEXT_SRC, src).build();
                        srcBuilder.add(jso);
                    }
                    JsonArray srcComponent = srcBuilder.build();
                    
                    JsonArrayBuilder hyperlinkSrcBuilder = Json.createArrayBuilder();
                    for(String hyperlinkSrc : textComponent.getHyperlinks()){
                        JsonObject jso = Json.createObjectBuilder().add(JSON_TEXT_HYPERLINK_SRC, hyperlinkSrc).build();
                        hyperlinkSrcBuilder.add(jso);
                    }
                    JsonArray hyperlinkSrcComponent = hyperlinkSrcBuilder.build();
                    
                    JsonArrayBuilder hyperlinkDistBuilder = Json.createArrayBuilder();
                    for(String hyperlinkDist : textComponent.getHyperlinkDists()){
                        JsonObject jso = Json.createObjectBuilder().add(JSON_TEXT_HYPERLINK_DIST, hyperlinkDist).build();
                        hyperlinkDistBuilder.add(jso);
                    }
                    JsonArray hyperlinkDistComponent = hyperlinkDistBuilder.build();
                    
                    if(textComponent.getSrcType().equals("Header")){
                        JsonObject jso = Json.createObjectBuilder().add(JSON_TEXT_TYPE, textComponent.getSrcType())
                                                               .add(JSON_TEXT_SRCS, srcComponent)
                                                               .add(JSON_TEXT_HYPERLINK_SRCS, hyperlinkSrcComponent)
                                                               .add(JSON_TEXT_HYPERLINK_DISTS, hyperlinkDistComponent)
                                                               .build();
                        jsb.add(jso);
                    }
                    else if(textComponent.getSrcType().equals("Paragraph")){
                        JsonObject jso = Json.createObjectBuilder().add(JSON_TEXT_TYPE, textComponent.getSrcType())
                                                               .add(JSON_TEXT_SRCS, srcComponent)
                                                               .add(JSON_TEXT_HYPERLINK_SRCS, hyperlinkSrcComponent)
                                                               .add(JSON_TEXT_HYPERLINK_DISTS, hyperlinkDistComponent)
                                                               .add(JSON_TEXT_FONT, textComponent.getFont())
                                                               .build();
                        jsb.add(jso);
                    }
                }
                else if(textComponent.getSrcType().equals("List")){
                    JsonArrayBuilder listBuilder = Json.createArrayBuilder();
                   
                    for(EportfolioList list: textComponent.getListSrcs()){
                        JsonArrayBuilder listSrcArrayBuilder = Json.createArrayBuilder();
                        for(String src : list.getSrcs()){
                            JsonObject jso = Json.createObjectBuilder().add(JSON_LIST_SRC, src).build();
                            listSrcArrayBuilder.add(jso);
                        }
                        JsonArray listSrcArray = listSrcArrayBuilder.build();
                        
                        JsonArrayBuilder listHyperlinkSrcArrayBuilder = Json.createArrayBuilder();
                        for(String src : list.getHyperlinkSrcs()){
                            JsonObject jso = Json.createObjectBuilder().add(JSON_LIST_HYPERLINK_SRC, src).build();
                            listHyperlinkSrcArrayBuilder.add(jso);
                        }
                        JsonArray listHyperlinkSrcArray = listHyperlinkSrcArrayBuilder.build();
                        
                        JsonArrayBuilder listHyperlinkDistArrayBuilder = Json.createArrayBuilder();
                        for(String src : list.getHyperlinkDists()){
                            JsonObject jso = Json.createObjectBuilder().add(JSON_LIST_HYPERLINK_DIST, src).build();
                            listHyperlinkDistArrayBuilder.add(jso);
                        }
                        JsonArray listHyperlinkDistArray = listHyperlinkDistArrayBuilder.build();
                        
                        JsonObject jso = Json.createObjectBuilder().add(JSON_LIST_SRCS, listSrcArray)
                                                                   .add(JSON_LIST_HYPERLINK_SRCS, listHyperlinkSrcArray)
                                                                   .add(JSON_LIST_HYPERLINK_DISTS, listHyperlinkDistArray)
                                                                   .build();
                        listBuilder.add(jso);
                    }
                    JsonArray listComponent = listBuilder.build();
                    JsonObject jso = Json.createObjectBuilder().add(JSON_TEXT_TYPE, textComponent.getSrcType())
                                                               .add(JSON_TEXT_SRCS, listComponent)
                                                               .build();
                    jsb.add(jso);
                }
                
            }break;
            case "image":{
                EportfolioImageComponent imageComponent = (EportfolioImageComponent)component;
                JsonObject jso = Json.createObjectBuilder().add(JSON_IMG_SRC, imageComponent.getImageSrc())
                                                           .add(JSON_IMG_CAPTION, imageComponent.getCaption())
                                                           .add(JSON_IMG_WIDTH, imageComponent.getWidth())
                                                           .add(JSON_IMG_HEIGHT, imageComponent.getHiehgt())
                                                           .add(JSON_IMG_OPTION, imageComponent.getImageOption())
                                                           .build();
                jsb.add(jso);
            }break;
            case "slideshow":{
                EportfolioSlideshowComponent slideShowComponent = (EportfolioSlideshowComponent)component;
                for(EportfolioSlide slide : slideShowComponent.getSrcs()){
                    JsonObject jso = Json.createObjectBuilder().add(JSON_SLIDE_SRC, slide.getSrc())
                                                               .add(JSON_SLIDE_CAPTION, slide.getCaption())
                                                               .build();
                    jsb.add(jso);
                }
                
            }break;
            case "video":{
                EportfolioVideoComponent videoComponent = (EportfolioVideoComponent)component;
                JsonObject jso = Json.createObjectBuilder().add(JSON_VIDEO_SRC, videoComponent.getVideoSrc())
                                                           .add(JSON_VIDEO_CAPTION, videoComponent.getCaption())
                                                           .add(JSON_VIDEO_WIDTH, videoComponent.getWidth())
                                                           .add(JSON_VIDEO_HEIHGT, videoComponent.getHeight())
                                                           .build();
                jsb.add(jso);
            }break;
                
        }
        
        JsonArray componentSrcs = jsb.build();
        
        JsonObject jso = Json.createObjectBuilder().add(JSON_COMPONENT_TYPE, component.getType())
                                                   .add(JSON_COMPONENT_SRCS, componentSrcs)
                                                   .build();
        return jso;
    }
}
