/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epg.file;

import static epg.StartupConstants.PATH_DATA;
import static epg.StartupConstants.PATH_WEB_VIEW;
import epg.model.EportfolioComponent;
import epg.model.EportfolioImageComponent;
import epg.model.EportfolioPage;
import epg.model.EportfolioPageModel;
import epg.model.EportfolioSlide;
import epg.model.EportfolioSlideshowComponent;
import epg.model.EportfolioVideoComponent;
import static epg.view.EportfolioWebViewSpace.SLASH;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.util.logging.Level;
import java.util.logging.Logger;
	import org.apache.commons.io.FileUtils;

/**
 *
 * @author wqu
 */
public class EportfolioExportManager {
    EportfolioPageModel pageModel;
    String path;
    File rootDir;
    File cssDir;
    File dataDir;
    File imgDir;
    File jsDir;
    File videoDir;
        public static String SLASH = "/";

    public EportfolioExportManager(EportfolioPageModel initPageModel, String initPath){
        try {
            pageModel = initPageModel;
            path = initPath;
            
            initDir();
            initData();
            initCss();
            initJs();
            initJsonData();
            initHTMLData();
            
        } catch (IOException ex) {
            Logger.getLogger(EportfolioExportManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void initDir() {
        rootDir = new File(path +SLASH+pageModel.getPages().get(0).getName()+"'s ePortfolio");
        cssDir = new File(rootDir.getPath()+SLASH + "css");
        dataDir = new File(rootDir.getPath()+SLASH + "data");
        imgDir = new File(rootDir.getPath()+SLASH + "img");
        jsDir = new File(rootDir.getPath()+SLASH + "js");
        videoDir = new File(rootDir.getPath()+SLASH + "video");
        
        if(rootDir.exists()){
            File[] entries = rootDir.listFiles();
            for(File f: entries){
                if(f.isDirectory()){
                    File[]data = f.listFiles();
                    for(File i :data){
                        i.delete();
                    }
                }
                f.delete();
            }
            rootDir.delete();
        }
        
        rootDir.mkdir();
        cssDir.mkdir();
        dataDir.mkdir();
        imgDir.mkdir();
        jsDir.mkdir();
        videoDir.mkdir();
    }

    private void initData() throws IOException {
        for(EportfolioPage page : pageModel.getPages()){
                    if(!page.getBannerImgSrc().equals("null")){
                    File bannerTemp = new File(page.getBannerImgSrc());
        File bannerImg = new File(imgDir.getPath()+ SLASH + page.getBannerImgSrc().substring(page.getBannerImgSrc().lastIndexOf("\\")+1));
        if(!bannerImg.exists()){
            Files.copy(bannerTemp.toPath(), bannerImg.toPath());
        }
                    }
            for(EportfolioComponent component : page.getComponents()){
            if (component.getType().equals("image")){
                File imgTemp = new File((((EportfolioImageComponent)component).getImageSrc()));
                File img = new File(imgDir.getPath()+SLASH+(((EportfolioImageComponent)component).getImageSrc()).substring((((EportfolioImageComponent)component).getImageSrc()).lastIndexOf("\\")+1));
                if(!img.exists()){
                    Files.copy(imgTemp.toPath(), img.toPath());
                }
            }
            else if(component.getType().equals("slideshow")){
                for(EportfolioSlide slide : ((EportfolioSlideshowComponent)component).getSrcs()){
                    File imgTemp = new File(slide.getSrc());
                    File img = new File(imgDir.getPath()+SLASH + slide.getSrc().substring(slide.getSrc().lastIndexOf("\\")+1));
                    if(!img.exists()){
                        Files.copy(imgTemp.toPath(), img.toPath());
                    }
                }
            }
            else if(component.getType().equals("video")){
                File videoTemp =new File(((EportfolioVideoComponent)component).getVideoSrc());
                File video = new File(videoDir.getPath()+SLASH+ ((EportfolioVideoComponent)component).getVideoSrc().substring(((EportfolioVideoComponent)component).getVideoSrc().lastIndexOf("\\")+1));
                if(!video.exists()){
                    Files.copy(videoTemp.toPath(), video.toPath());
                }
            }
        }
        }
    }

    private void initCss() throws IOException {
        File cssTemp = new File(PATH_WEB_VIEW+SLASH +"css");
        File cssDist = new File(cssDir.getPath());
        FileUtils.copyDirectory(cssTemp, cssDist);
    }

    private void initJs() throws IOException {
        File jsTemp = new File(PATH_DATA+ SLASH + "export_data/js/jsonControl.js");
        File jsDist = new File(jsDir.getPath()+SLASH+"jsonControl.js");
        if(!jsDist.exists()){
            Files.copy(jsTemp.toPath(), jsDist.toPath());
        }
    }

    private void initJsonData() throws FileNotFoundException {
        for(EportfolioPage page : pageModel.getPages()){
            EportfolioWebJsonGenerator jsonGenerator = new EportfolioWebJsonGenerator();
            jsonGenerator.generateWebViewJson(page, dataDir.getPath()+SLASH+page.getTitle()+".json");
        }
    }

    private void initHTMLData() throws IOException {
        for(EportfolioPage page: pageModel.getPages()){
            File HTMLTemp = new File(PATH_DATA +SLASH+ "export_data/template.html");
            File HTMLDist = new File(rootDir.getPath()+SLASH+page.getTitle()+".html");
            if(!HTMLDist.exists()){
                Files.copy(HTMLTemp.toPath(), HTMLDist.toPath());
            }
        }
    }
    
}
