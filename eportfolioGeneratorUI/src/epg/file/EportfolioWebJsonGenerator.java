/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epg.file;

import static epg.StartupConstants.PATH_WEB_VIEW;
import static epg.file.EportfolioGeneratorFileManager.JSON_PAGE_BANNER_IMG_SRC;
import static epg.file.EportfolioGeneratorFileManager.JSON_PAGE_COLOR_TEMPALTE;
import static epg.file.EportfolioGeneratorFileManager.JSON_PAGE_COMPONENTS;
import static epg.file.EportfolioGeneratorFileManager.JSON_PAGE_FONT_TEMPALTE;
import static epg.file.EportfolioGeneratorFileManager.JSON_PAGE_FOOTER_NOTES;
import static epg.file.EportfolioGeneratorFileManager.JSON_PAGE_INDEX;
import static epg.file.EportfolioGeneratorFileManager.JSON_PAGE_LAYOUT_TEMPLATE;
import static epg.file.EportfolioGeneratorFileManager.JSON_PAGE_STUDENT_NAME;
import static epg.file.EportfolioGeneratorFileManager.JSON_PAGE_TITLE;
import epg.model.EportfolioComponent;
import epg.model.EportfolioPage;
import epg.model.EportfolioPageModel;
import epg.view.EportfolioGeneratorView;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonWriter;
import properties_manager.PropertiesManager;

/**
 *
 * @author wqu
 */
public class EportfolioWebJsonGenerator {
    public static String JSON_PAGE_NAVIGATION = "navigation";
    public static String JSON_NAVIGATION_SRC = "navigation_src";
    public static String JSON_NAVIGATION_DIST = "navigation_dist";
    
    private EportfolioGeneratorView ui;
    /*        public  EportfolioWebJsonGenerator(EportfolioPageModel pageModel, String path, EportfolioGeneratorFileManager fileManager) throws IOException {
    //String path = PATH_WEB_VIEW +"sample.json";
    PropertiesManager props = PropertiesManager.getPropertiesManager();
    
    OutputStream os = new FileOutputStream (path);
    JsonWriter jsonWriter = Json.createWriter(os);
    
    JsonArray pageJsonArray = makeEportfolioJsonArray(pageModel.getPages());
    
    JsonObject pageJsonObject = Json.createObjectBuilder()
    .add(JSON_PAGE_INDEX, pageJsonArray)
    .build();
    
    //Gson prettyGson = new GsonBuilder().setPrettyPrinting().create();
    //String test = prettyGson.toJson(pageJsonObject);
    jsonWriter.writeObject(pageJsonObject);
    }*/
        public void generateWebViewJson(EportfolioPage page, String path) throws FileNotFoundException{
            jsonGenerator(page,path);
        }
        private void jsonGenerator(EportfolioPage page, String path) throws FileNotFoundException{
            PropertiesManager props = PropertiesManager.getPropertiesManager();
            
            OutputStream os = new FileOutputStream(path);
            JsonWriter jsonWriter = Json.createWriter(os);
            
            JsonArrayBuilder navigationBuilder = Json.createArrayBuilder();
            for(String pageTitle : page.getNavigation()){
                JsonObject jso = Json.createObjectBuilder().add(JSON_NAVIGATION_SRC,pageTitle)
                                                           .add(JSON_NAVIGATION_DIST, pageTitle+".html")
                                                           .build();
                navigationBuilder.add(jso);
            }
            JsonArray navigationArray = navigationBuilder.build();
            
            JsonArrayBuilder componentBuilder = Json.createArrayBuilder();
            EportfolioMakeJsonObject jsonObjectMaker = new EportfolioMakeJsonObject();
            for(EportfolioComponent component: page.getComponents()){
                JsonObject jso = jsonObjectMaker.jsonObjectMaker(component);
                componentBuilder.add(jso);
            }
            JsonArray componentArray = componentBuilder.build();
            JsonObject jso = Json.createObjectBuilder().add(JSON_PAGE_TITLE, page.getTitle())
                                                   .add(JSON_PAGE_STUDENT_NAME, page.getName())
                                                   .add(JSON_PAGE_BANNER_IMG_SRC, page.getBannerImgSrc())
                                                   .add(JSON_PAGE_LAYOUT_TEMPLATE, page.getTemplate())
                                                   .add(JSON_PAGE_COLOR_TEMPALTE, page.getColor())
                                                   .add(JSON_PAGE_FONT_TEMPALTE, page.getFont())
                                                   .add(JSON_PAGE_FOOTER_NOTES, page.getFooter() )
                                                   .add((JSON_PAGE_NAVIGATION), navigationArray)
                                                   .add(JSON_PAGE_COMPONENTS,componentArray )
                                                   .build();
            jsonWriter.writeObject(jso);
            
            
        }
}
