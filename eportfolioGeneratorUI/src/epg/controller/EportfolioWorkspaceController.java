/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epg.controller;

import epg.model.EportfolioComponent;
import epg.model.EportfolioPage;
import epg.model.EportfolioTextComponent;
import epg.view.EportfolioWorkspace;
import java.io.File;
import javafx.stage.FileChooser;

/**
 *
 * @author wqu
 */
public class EportfolioWorkspaceController {
    private EportfolioWorkspace workspaceUI;
    private EportfolioPage page;
    public EportfolioWorkspaceController(EportfolioWorkspace initWorkspaceUI) {
        workspaceUI = initWorkspaceUI;
        page = workspaceUI.getPageData();
    }

    public void handleAddTextRequest() {
        page.addComponent("text");
        workspaceUI.reloadPageComponent();
    }

    public void handleAddImageRequest() {
                page.addComponent("image");
        workspaceUI.reloadPageComponent();
    }

    public void handleAddSlideshowRequest() {
            page.addComponent("slideshow");
                    workspaceUI.reloadPageComponent();

    }

    public void handleAddVideoRequest() {
        page.addComponent("video");
                workspaceUI.reloadPageComponent();

    }

    public void handleEditRequest() {
            page.editComponent();
            workspaceUI.reloadPageComponent();
    }

    public void handleRemoveRequest() {
        page.removeComponent();
        workspaceUI.reloadPageComponent();
    }

    public void handleSetBannerRequest() {
        File bannerImg;
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Choice Banner Image");
        fileChooser.setInitialDirectory(new File(System.getProperty("user.home")));
        
        fileChooser.getExtensionFilters().addAll(
            new FileChooser.ExtensionFilter("All Images", "*.*"),
            new FileChooser.ExtensionFilter("JPG", "*.jpg"),
            new FileChooser.ExtensionFilter("PNG", "*.png")
        );
        bannerImg = fileChooser.showOpenDialog(null);
        if(bannerImg != null){
             workspaceUI.getPageData().setBannerImgSrc(bannerImg.getPath());
        }
        
    }


    public void handleSelectedRequest(EportfolioComponent component) {
        page.setSelectedComponent(component);
        workspaceUI.updateControls();
        workspaceUI.reloadPageComponent();
    }

    public void handleEditHyperlinkRequest() {
        if(page.getSelectedComponent().getType().equals("text")){
                ((EportfolioTextComponent)page.getSelectedComponent()).hyperlinkManager();
        
        }
                workspaceUI.reloadPageComponent();

    }
    
}
