/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epg.controller;

import epg.model.EportfolioPage;
import epg.model.EportfolioPageModel;
import epg.view.EportfolioGeneratorView;
import properties_manager.PropertiesManager;

/**
 *
 * @author Wenjun
 */
public class EportfolioGeneratorPageController {
    private EportfolioGeneratorView ui;
    public EportfolioGeneratorPageController(EportfolioGeneratorView initUI) {
        ui = initUI;
    }

    public void handleAddPageRequest() {
        EportfolioPageModel pageModel = ui.getPages();
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String test = "test";
        pageModel.addPage(test);
    }

    public void processSelectedPage(EportfolioPageModel pageModel, EportfolioPage page, EportfolioGeneratorView aThis) {
            pageModel.setSelected(page);
            ui.updatePageControlToolBar(pageModel);
            ui.reloadPagePane(pageModel);
            ui.reloadPageWorkSpace(page);
            ui.reloadWebViewSpace(page);
    }

    public void handleRemovePageRequest() {
        EportfolioPageModel pageModel = ui.getPages();
        pageModel.removeSelectedPage();
    }
    
}
