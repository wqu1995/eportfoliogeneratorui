/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epg.controller;

import static epg.StartupConstants.PATH_DATA;
import epg.file.EportfolioGeneratorFileManager;
import epg.file.EportfolioExportManager;
import epg.model.EportfolioPageModel;
import epg.view.EportfolioGeneratorView;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import properties_manager.PropertiesManager;

/**
 *
 * @author wqu
 */
public class EportfolioGeneratorFileController {
    private boolean saved;
    private EportfolioGeneratorView ui;
    private EportfolioGeneratorFileManager eportfolioIO;
    private String savePath;
    private String loadPath;
    
    public EportfolioGeneratorFileController(EportfolioGeneratorView initUI, EportfolioGeneratorFileManager initFileManager) {
        saved = true;
        ui = initUI;
        eportfolioIO = initFileManager;
    }
    
    public void markAsEdited(){
        saved = false; 
        ui.updateToolbarControls(saved);
    }

    public void handleExitWorkRequest() {
        System.exit(0);
    }

    public void handleNewWorkRequest() {
        EportfolioPageModel pageModel = ui.getPages();
        pageModel.reset();
        saved = false;
        ui.updateToolbarControls(saved);
        ui.reloadPagePane(pageModel);
    }
    
    public void handleSaveWorkRequest(){
        EportfolioPageModel pageModel = ui.getPages();
        if ((savePath == null) || (savePath.equals("fail"))){
            savePath = saveSelector();
        }
        if(!savePath.equals("fail")){
        try {
            eportfolioIO.saveWork(pageModel, savePath);
        } catch (IOException ex) {
            Logger.getLogger(EportfolioGeneratorFileController.class.getName()).log(Level.SEVERE, null, ex);
        }
        }
        saved =true;
        ui.updateToolbarControls(saved);
        ui.reloadPagePane(pageModel);
        System.out.println("saved");
    }
    
    public void handleSaveAsWorkRequest() {
                EportfolioPageModel pageModel = ui.getPages();
         savePath = saveSelector();
        if(!savePath.equals("fail")){
        try {
            eportfolioIO.saveWork(pageModel, savePath);
        } catch (IOException ex) {
            Logger.getLogger(EportfolioGeneratorFileController.class.getName()).log(Level.SEVERE, null, ex);
        }
        }
                saved =true;
        ui.updateToolbarControls(saved);
        ui.reloadPagePane(pageModel);
    }

    public void handleLoadWorkRequest() {
        EportfolioPageModel pageModel = ui.getPages();
        loadPath = loadSelector();
        if(!loadPath.equals("fail")){
        try {
            eportfolioIO.loadWork(pageModel, loadPath);
        } catch (IOException ex) {
            Logger.getLogger(EportfolioGeneratorFileController.class.getName()).log(Level.SEVERE, null, ex);
        }

        }
        saved = true;
        ui.updateToolbarControls(saved);
                    ui.reloadPagePane(pageModel);

    }
    
    public void handleExportWorkRequest(){
        EportfolioPageModel pageModel = ui.getPages();
        DirectoryChooser chooser = new DirectoryChooser();
        chooser.setTitle("Export ePortfolio data");
        chooser.setInitialDirectory(new File(PATH_DATA));
        File file = chooser.showDialog(null);
        if(file !=null){
            EportfolioExportManager exportManager = new EportfolioExportManager(pageModel, file.getPath());
        }
        else if(file == null){
        }
        else{
        }
    }

    private String saveSelector() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Save ePortfolio data");
        fileChooser.setInitialDirectory(new File(PATH_DATA));
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("JSON data file(*.json)", "*.json"));
        File file = fileChooser.showSaveDialog(null);
        if(file != null){
            return file.getPath();
        }
        else if(file == null){
            return "fail";
        }
        else{
            return null;
        }
    }

    private String loadSelector() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Save ePortfolio data");
        fileChooser.setInitialDirectory(new File(PATH_DATA));
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("JSON data file(*.json)", "*.json"));
        File file = fileChooser.showOpenDialog(null);
        if(file !=null){
            return file.getPath();
        }
        else if(file == null){
            return "fail";
        }
        else{
            return null;
        }
    }


    
}
