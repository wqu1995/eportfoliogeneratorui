/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epg;

import static epg.LanguagePropertyType.TITLE_WINDOW;
import epg.error.ErrorHandler;
import epg.file.EportfolioGeneratorFileManager;
import epg.view.EportfolioGeneratorView;
import javafx.application.Application;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;
import static epg.StartupConstants.PATH_DATA;
import static epg.StartupConstants.PROPERTIES_SCHEMA_FILE_NAME;
import static epg.StartupConstants.UI_PROPERTIES_FILE_NAME_EN;
import xml_utilities.InvalidXMLFileFormatException;

/**
 *
 * @author wqu
 */
public class EportfolioGeneratorUI extends Application {
    EportfolioGeneratorFileManager fileManager = new EportfolioGeneratorFileManager();
    EportfolioGeneratorView ui = new EportfolioGeneratorView(fileManager);
    
    @Override
    public void start(Stage primaryStage) {
       System.setProperty("glass.accessible.force", "false");
        boolean success = loadProperties();
        if(success){
            PropertiesManager props = PropertiesManager.getPropertiesManager();
            String appTitle = props.getProperty(TITLE_WINDOW);
            ui.startUI(primaryStage, appTitle);
        }
        else{
	    ErrorHandler errorHandler = ui.getErrorHandler();
	    errorHandler.processError(LanguagePropertyType.ERROR_SYSTEM_FAIL, LanguagePropertyType.ERROR_TITLE, LanguagePropertyType.ERROR_DATA_FILE_LOADING);
	    System.exit(0);            
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

    public boolean loadProperties() {
        try{
            PropertiesManager props = PropertiesManager.getPropertiesManager();
            props.addProperty(PropertiesManager.DATA_PATH_PROPERTY, PATH_DATA);
            props.loadProperties(UI_PROPERTIES_FILE_NAME_EN, PROPERTIES_SCHEMA_FILE_NAME);
            return true;
        }catch (InvalidXMLFileFormatException ixmlffe) {
            // SOMETHING WENT WRONG INITIALIZING THE XML FILE
           
            ErrorHandler eH = ui.getErrorHandler();
            eH.processError(LanguagePropertyType.ERROR_XML_FILE_FAIL, LanguagePropertyType.ERROR_TITLE, LanguagePropertyType.ERROR_PROPERTIES_FILE_LOADING);
            System.exit(0);

            return false;
        } 
    }
    
}
